package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Activity_MakeUp extends AppCompatActivity {
    //category
    private ImageView Cheek;
    private ImageView Lips;
    private ImageView Face;
    private ImageView Cleanser;

    //new offer
    private ImageView Emina;
    private ImageView Loreal;
    private ImageView BioAqua;
    private ImageView Spot;

    //search
    private ImageView cari;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_up);

        //Menu Utama
        Cheek = (ImageView) findViewById(R.id.Cheek);
        Cheek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                produkcategorymakeup();
            }
        });
        Lips = (ImageView) findViewById(R.id.Lips);
        Lips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                produkcategorymakeup();
            }
        });
        Face = (ImageView) findViewById(R.id.Face);
        Face.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                produkcategorymakeup();
            }
        });
        Cleanser = (ImageView) findViewById(R.id.Cleanser);
        Cleanser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                produkcategorymakeup();
            }
        });

        //New Offer
        Emina = (ImageView) findViewById(R.id.Emina);
        Emina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detail();
            }
        });
        Loreal = (ImageView) findViewById(R.id.Loreal);
        Loreal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detail();
            }
        });

        //Hot Dealer
        BioAqua = (ImageView) findViewById(R.id.BioAqua);
        BioAqua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detail();
            }
        });
        Spot = (ImageView) findViewById(R.id.Spot);
        Spot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detail();
            }
        });

        //search
        cari = (ImageView) findViewById(R.id.cari);
        cari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search();
            }
        });
    }

    //Menu Utama Make Up
    public void produkcategorymakeup(){
        Intent intent = new Intent(this, Activity_ProdukCategoryMakeUp.class);
        startActivity(intent);
    }

    //detail produk
    public void detail(){
        Intent intent = new Intent(this, Activity_DetailProdukMakeUp.class);
        startActivity(intent);
    }

    //search
    public void search(){
        Intent intent = new Intent(this, PencarianSearchMakeUp.class);
        startActivity(intent);
    }
}
