package com.example.girlthings;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.girlthings.adapter.DaftarStylistAdapter;
import com.example.girlthings.model.DaftarStylistModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class DaftarStylist extends AppCompatActivity{
    private RecyclerView mRvDaftarStylist;
    private DaftarStylistAdapter adapter;
    private List<DaftarStylistModel> models = new ArrayList<>();
    private FirebaseDatabase database;
    private DatabaseReference dbRef;
    private String type;
    private Long dataCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_stylist);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        type = getIntent().getStringExtra("stylist");

        database = FirebaseDatabase.getInstance();
        dbRef = database.getReference("expert");
        mRvDaftarStylist = findViewById(R.id.rvDaftarStylist);

        crateListData();
    }

    private void crateListData() {
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    dataCount = dataSnapshot.getChildrenCount();
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        DaftarStylistModel model = ds.getValue(DaftarStylistModel.class);
                        models.add(model);
                    }
                    mRvDaftarStylist.setLayoutManager(new LinearLayoutManager(DaftarStylist.this));
                    adapter = new DaftarStylistAdapter(DaftarStylist.this, models);
                    mRvDaftarStylist.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Do Nothing
            }
        });
    }
}
