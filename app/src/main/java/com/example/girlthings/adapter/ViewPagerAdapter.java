package com.example.girlthings.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.girlthings.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {

    Context context;
    List<String> slider;
    LayoutInflater layoutInflater;

    public ViewPagerAdapter(Context context, List<String> slider) {
        this.context = context;
        this.slider = slider;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return slider.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = layoutInflater.inflate(R.layout.viewpager_image, null);
        final ProgressBar progressBar = view.findViewById(R.id.viewPagerProgressBar);
        final ImageView image = view.findViewById(R.id.viewPagerImage);
        final ImageView imageBackground = view.findViewById(R.id.viewPagerBackground);

        container.addView(view);

        try {
            Picasso.with(context)
                    .load(slider.get(position).trim())
                    .error(R.drawable.upload)
                    .into(image, new Callback() {
                        @Override
                        public void onSuccess() {
                            imageBackground.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            imageBackground.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}
