package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import static com.example.girlthings.Activity_DetailProdukMakeUp.EXTRA_MESSAGE;

public class Activity_TransferMakeUp extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.android.twoactivities.extra.MESSAGE";
    public static final String EXTRA_MESSAGE_2 = "com.example.android.twoactivities.extra.MESSAGE_2";

    private TextView jumlahharga;
    String bank;
    Button Konfirmasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_make_up);

        jumlahharga = (TextView)findViewById(R.id.jumlahharga);

        Konfirmasi = (Button) findViewById(R.id.buttonKonfirmasi);
        Konfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Konfirmasi();
            }
        });

        Intent intent = getIntent();

        bank = intent.getStringExtra(EXTRA_MESSAGE);
        String jumlah = intent.getStringExtra(EXTRA_MESSAGE);

        TextView nomorrekening = findViewById(R.id.nomorrekening);
        TextView textView = findViewById(R.id.jumlahharga);
        textView.setText(jumlah);

        if (bank.matches("Mandiri")){
            nomorrekening.setText("147-1000-2456-1231");
        }else if (bank.matches("BCA")){
            nomorrekening.setText("256-2000-421-9987");
        }else{
            nomorrekening.setText("678-4000-1294-4215");
        }
    }
    public void Konfirmasi(){
        String coba =  jumlahharga.getText().toString();
        Intent intent = new Intent(this, Activity_KonfirmasiPembayaranMakeUp.class);
        intent.putExtra(EXTRA_MESSAGE, coba);
        startActivity(intent);
    }
}
