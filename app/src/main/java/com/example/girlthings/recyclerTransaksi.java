package com.example.girlthings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class recyclerTransaksi extends AppCompatActivity {
    private RecyclerView mResultList;
    private DatabaseReference mDatabaseReference;
    FirebaseRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_transaksi);
        mDatabaseReference = FirebaseDatabase.getInstance().getReference("transaksiStatus");
        mResultList =  findViewById(R.id.reTransaksi);
        firebaseUserView();
    }

    private void firebaseUserView() {
        //Query query = mDatabaseReference.orderByChild("username").startAt(searchText).endAt(searchText + "\uf8ff");

        FirebaseRecyclerOptions<transaksiStatus> options =
                new FirebaseRecyclerOptions.Builder<transaksiStatus>()
                        .setQuery(mDatabaseReference, transaksiStatus.class)
                        .build();
        adapter = new FirebaseRecyclerAdapter<transaksiStatus, TransaksiViewHolder>(options) {
            @NonNull
            @Override
            public TransaksiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_view_transaksi, parent, false);
                TransaksiViewHolder viewHolder = new TransaksiViewHolder(view);
                return viewHolder;
            }

            @Override
            protected void onBindViewHolder(@NonNull TransaksiViewHolder holder, int i, @NonNull transaksiStatus model) {
                holder.traId.setText(model.getIdTra());
                holder.traBarang.setText(model.getBarang());
                holder.traTotal.setText("Rp. "+model.getTotal());
                if (String.valueOf(model.getStatus()).matches("Dibayar")) {
                    holder.traStatus.setBackgroundResource(R.drawable.statushijau);
                }else if (String.valueOf(model.getStatus()).matches("Dikirim")){
                    holder.traStatus.setBackgroundResource(R.drawable.statusbiru);
                }
                holder.traStatus.setText(model.getStatus());
            }
        };
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(this);
        mResultList.setLayoutManager(gridLayoutManager);
        adapter.startListening();
        mResultList.setAdapter(adapter);
    }

    public static class TransaksiViewHolder extends RecyclerView.ViewHolder {

        View mView;
        TextView traBarang, traTotal, traId, traStatus;
        public TransaksiViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            traId = (TextView) mView.findViewById(R.id.idTra);
            traBarang = (TextView) mView.findViewById(R.id.namaTransaksi);
            traTotal = (TextView) mView.findViewById(R.id.totalTransaksi);
            traStatus = (TextView) mView.findViewById(R.id.statusTra);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if(adapter!=null){
            adapter.startListening();
        }
    }

    @Override
    protected void onStop() {
        if(adapter!=null){
            adapter.stopListening();
        }
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(adapter!=null){
            adapter.startListening();
        }
    }
}
