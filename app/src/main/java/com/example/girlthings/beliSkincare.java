package com.example.girlthings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.girlthings.model.UserModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class beliSkincare extends AppCompatActivity {
    public static final String hargaSkin= "hargaSkin";
    public static final String namaSkin= "namaSkin";
    public static final String jmlSkin= "jmlSkin";
    Button beli, keranjang;
    EditText jumlah;
    int total, total1;
    private FirebaseAuth mAuth;
    private FirebaseDatabase fDb;
    private DatabaseReference dbRef;
    private String uid;
    private UserModel userModel;
    TextView produk, harga;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beli_skincare);
        beli = (Button) findViewById(R.id.button);
        keranjang = (Button) findViewById(R.id.button2);
        jumlah = (EditText) findViewById(R.id.editText);


        total = 45000;
        beli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toTotal();
            }
        });

        mAuth = FirebaseAuth.getInstance();
        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference();
        mAuth = FirebaseAuth.getInstance();
        uid = mAuth.getUid();
        dbRef.child("users").child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userModel = dataSnapshot.getValue(UserModel.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Do Nothing
            }
        });

        keranjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertToCart();
            }
        });
    }

    public void toTotal() {
        String coba2 = String.valueOf(total);
        String coba = jumlah.getText().toString();
        String coba3 = "Day Cream";
        Intent intent = new Intent(this, totalpembelianbelipakaian.class);
        intent.putExtra(hargaSkin, coba2);
        intent.putExtra(jmlSkin, coba);
        intent.putExtra(namaSkin, coba3);
        startActivity(intent);
    }

    private void insertToCart() {
        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference mDbRef = mDatabase.getReference().child("Cart");

        String name = "Day Cream";
        String total = jumlah.getText().toString();
        String hargaPro = "45000";


        String numberOnly= hargaPro.replaceAll("[^0-9]", "");
        total1 = Integer.parseInt(total)*Integer.parseInt(numberOnly);

        String user = "alex";

        Cart cart = new Cart(userModel.getName(), name, user, String.valueOf(total1), total);
        mDbRef.push().setValue(cart);

        Toast.makeText(beliSkincare.this, "Berhasil Dimasukkan Keranjang", Toast.LENGTH_LONG).show();
    }
}
