package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.girlthings.model.ConfirmationModel;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;

public class ConfirmationPaymentActivity extends AppCompatActivity {

    private ImageView imageConfirmation;

    public Uri imguri;
    private StorageReference mStorageRef;
    private FirebaseDatabase fDb;
    private DatabaseReference dbRef;
    private ConfirmationModel consModel;

    private int transId;
    private int confsId;
    private String username;
    private String price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation_payment);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        transId = getIntent().getIntExtra("transId", 0);
        confsId = getIntent().getIntExtra("confsId", 0);
        username = getIntent().getStringExtra("username");
        price = getIntent().getStringExtra("price");

        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference("transaksiStatus");
        mStorageRef = FirebaseStorage.getInstance().getReference().child("TransactionImageFolder");

        imageConfirmation = findViewById(R.id.imageConfirmation);
    }

    public void chooseImage(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, 1);
    }

    public void sendConfirmation(View view) {
        final StorageReference Ref = mStorageRef.child("image" + imguri.getLastPathSegment());
        Ref.putFile(imguri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        consModel = new ConfirmationModel(transId, "Pending", String.valueOf(uri), username, price, "Make Up Artist");
                        dbRef.child(confsId + "").setValue(consModel);
                    }
                });
            }
        });
        startActivity(new Intent(this, HomeActivity.class));
        Toast.makeText(this, "Konfirmasi berhasil, silahkan menunggu konfirmasi dari admin", Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                imguri = data.getData();
                imageConfirmation.setVisibility(View.VISIBLE);
                imageConfirmation.setImageURI(imguri);
            }
        }
    }

    public void onExit(View view) {
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }
}
