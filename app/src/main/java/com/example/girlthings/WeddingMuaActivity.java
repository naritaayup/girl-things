package com.example.girlthings;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.girlthings.adapter.WeddingMakeUpArtistAdapter;
import com.example.girlthings.model.WeddingMakeUpArtistModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class WeddingMuaActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Spinner spinner;
    private RecyclerView recyclerViewWeddingMakeUpArtist;
    private WeddingMakeUpArtistAdapter adapter;
    private List<WeddingMakeUpArtistModel> models = new ArrayList<>();
    private FirebaseDatabase database;
    private DatabaseReference dbRef;
    private String type;
    private Long dataCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wedding_mua);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        type = getIntent().getStringExtra("wedding");

        database = FirebaseDatabase.getInstance();
        dbRef = database.getReference("mua");
        recyclerViewWeddingMakeUpArtist = findViewById(R.id.recyclerViewWeddingMakeUpArtist);
        spinner = findViewById(R.id.spinner);

        crateListData();

        if (spinner != null) {
            spinner.setOnItemSelectedListener(this);
        }

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.labels_array,
                android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        if (spinner != null) {
            spinner.setAdapter(adapter);
        }
    }

    private void crateListData() {
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    dataCount = dataSnapshot.getChildrenCount();
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        WeddingMakeUpArtistModel model = ds.getValue(WeddingMakeUpArtistModel.class);
                        models.add(model);
                    }
                    recyclerViewWeddingMakeUpArtist.setLayoutManager(new LinearLayoutManager(WeddingMuaActivity.this));
                    adapter = new WeddingMakeUpArtistAdapter(WeddingMuaActivity.this, models, type);
                    recyclerViewWeddingMakeUpArtist.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    adapter.getFilter().filter(type);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Do Nothing
            }
        });
    }

    public void onBackPressed(View view) {
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        getSelectedLabel(position);
    }

    private void getSelectedLabel(int position) {
        if (position == 1) {
            adapter.getFilter().filter("Bandung Barat");
        } else if (position == 2) {
            adapter.getFilter().filter("Bandung Utara");
        } else if (position == 3) {
            adapter.getFilter().filter("Bandung Selatan");
        } else if (position == 4) {
            adapter.getFilter().filter(type);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // Do Nothing
    }
}
