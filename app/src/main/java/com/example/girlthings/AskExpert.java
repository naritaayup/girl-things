package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AskExpert extends AppCompatActivity {
    CardView mCvDokter, mCvDesigner, mCvStylist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_expert);

        mCvDokter = (CardView) findViewById(R.id.cvDokter);
        mCvDokter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dokterKecantikan = new Intent(getApplicationContext(),DokterKecantikan.class);
                startActivity(dokterKecantikan);
            }
        });

        mCvDesigner = (CardView) findViewById(R.id.cvDesigner);
        mCvDesigner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fashionDesigner = new Intent(getApplicationContext(),FashionDesigner.class);
                startActivity(fashionDesigner);
            }
        });

        mCvStylist = (CardView) findViewById(R.id.cvStylist);
        mCvStylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fashionStylist = new Intent(getApplicationContext(),FashionStylist.class);
                startActivity(fashionStylist);
            }
        });
    }
}
