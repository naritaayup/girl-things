package com.example.girlthings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

public class Activity_KonfirmasiPembayaranMakeUp extends AppCompatActivity {

    Button UploadFile,Kirim;
    TextView choosefile;
    ImageView Image;
    StorageReference mStorageRef;

    private StorageTask UploadTask;
    public Uri imguri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konfirmasi_pembayaran_make_up);
        mStorageRef = FirebaseStorage.getInstance().getReference("Images");

        choosefile= (TextView) findViewById(R.id.choosefile);
        choosefile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filechoose();
            }
        });

        UploadFile = (Button) findViewById(R.id.uploadfile);
        UploadFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UploadTask != null && UploadTask.isInProgress()){
                    Toast.makeText(Activity_KonfirmasiPembayaranMakeUp.this, "Upload Is In Progress", Toast.LENGTH_LONG).show();
                }else{
                    fileuploader();
                }
            }
        });
        Image = (ImageView) findViewById(R.id.imageView12);

        Kirim = (Button) findViewById(R.id.Kirim);
        Kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Berhasil();
            }
        });
    }

    public void fileuploader(){
        StorageReference Ref = mStorageRef.child(System.currentTimeMillis()+"."+getExtension(imguri));
        UploadTask = Ref.putFile(imguri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        Uri downloadUrl = Uri.parse(taskSnapshot.getMetadata().getReference().getDownloadUrl().toString());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        // ...
                    }
                });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1  && requestCode==RESULT_OK && data!=null && data.getData()!=null){
            imguri = data.getData();
            Image.setImageURI(imguri);
        }
    }

    private String getExtension (Uri uri){
        ContentResolver cr = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(cr.getType(uri));
    }

    public void filechoose(){
        Intent intent = new Intent();
        intent.setType("Image/ *");
        intent.setAction(intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,1);
    }

    public void Berhasil(){
        Intent intent = new Intent(this, Activity_PembayaranBerhasilMakeUp.class);
        startActivity(intent);
    }
}
