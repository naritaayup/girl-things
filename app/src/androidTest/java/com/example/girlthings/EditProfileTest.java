package com.example.girlthings;

import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class EditProfileTest {
    String nama="Rangga Ajha";
    String bio="Halo! semoga bahagia selalu!";
    String phone="089502551212";
    String email="ranggaajha@gmail.com";
    String alamat="Jl. Bojongsoang no. XXX";


    @Rule
    public ActivityTestRule<EditProfile> activityRule =
            new ActivityTestRule<EditProfile>(EditProfile.class);

    @Test
    public void checkGagal() throws Exception {
        onView(withId(R.id.etNama)).perform(typeText(nama), closeSoftKeyboard());
        onView(withId(R.id.etBio)).perform(typeText(bio), closeSoftKeyboard());
        onView(withId(R.id.etNoTelp)).perform(typeText(phone), closeSoftKeyboard());
        onView(withId(R.id.etEmail)).perform(typeText(email), closeSoftKeyboard());
        onView(withId(R.id.etAlamat)).perform(typeText(alamat), closeSoftKeyboard());
        onView(withId(R.id.btnSimpan)).perform(click());
    }

    public void checkBerhasil() throws Exception {
        onView(withId(R.id.etNama)).perform(typeText(nama), closeSoftKeyboard());
        onView(withId(R.id.etBio)).perform(typeText(bio), closeSoftKeyboard());
        onView(withId(R.id.etNoTelp)).perform(typeText(phone), closeSoftKeyboard());
        onView(withId(R.id.etEmail)).perform(typeText(email), closeSoftKeyboard());
        onView(withId(R.id.etAlamat)).perform(typeText(alamat), closeSoftKeyboard());
        onView(withId(R.id.btnSimpan)).perform(click());
    }
}
