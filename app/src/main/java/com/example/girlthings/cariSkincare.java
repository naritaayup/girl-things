package com.example.girlthings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.girlthings.model.SkinCare;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

public class cariSkincare extends AppCompatActivity {

    private RecyclerView mResultList;
    private EditText searchField;
    private ImageView searchImage;
    private DatabaseReference mPakaianDatabase;
    FirebaseRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cari_skincare);

        searchField = (EditText) findViewById(R.id.find);
        searchImage = (ImageView) findViewById(R.id.cari);

        mPakaianDatabase = FirebaseDatabase.getInstance().getReference("SkinCare");

        mResultList = (RecyclerView) findViewById(R.id.result_list);

        searchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchText = searchField.getText().toString();
                firebaseUserSearch(searchText);
            }
        });

    }

    private void firebaseUserSearch(String searchText) {

        Toast.makeText(cariSkincare.this, "Started Search", Toast.LENGTH_LONG).show();

        Query query = mPakaianDatabase.orderByChild("nama").startAt(searchText).endAt(searchText + "\uf8ff");

        FirebaseRecyclerOptions<SkinCare> options =
                new FirebaseRecyclerOptions.Builder<SkinCare>()
                        .setQuery(query, SkinCare.class)
                        .build();
        adapter = new FirebaseRecyclerAdapter<SkinCare, UsersViewHolder>(options){
            @NonNull
            @Override
            public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_recycler_cari, parent, false);
                UsersViewHolder viewHolder = new UsersViewHolder(view);
                return viewHolder;
            }
            @Override
            protected void onBindViewHolder(@NonNull UsersViewHolder holder, int position, @NonNull SkinCare model) {
                holder.SkinCare_nama.setText(model.getNama());
                holder.SkinCare_desc.setText(model.getDeskripsi());
                holder.SkinCare_harga.setText(model.getHarga());
                String link = String.valueOf(model.getGambar());
                //Picasso.get().load(link).into(holder.pakaian_gambar);
                Picasso.with(cariSkincare.this).load(link).into(holder.SkinCare_gambar);
            }
        };
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(this);
        mResultList.setLayoutManager(gridLayoutManager);
        adapter.startListening();
        mResultList.setAdapter(adapter);
    }

    public static class UsersViewHolder extends RecyclerView.ViewHolder {

        View mView;
        TextView SkinCare_nama, SkinCare_harga, SkinCare_desc;
        ImageView SkinCare_gambar;
        public UsersViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            SkinCare_nama = (TextView) mView.findViewById(R.id.name);
            SkinCare_harga = (TextView) mView.findViewById(R.id.price);
            SkinCare_desc = (TextView) mView.findViewById(R.id.desc);
            SkinCare_gambar = (ImageView) mView.findViewById(R.id.image);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(adapter!=null){
            adapter.startListening();
        }
    }

    @Override
    protected void onStop() {
        if(adapter!=null){
            adapter.stopListening();
        }
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(adapter!=null){
            adapter.startListening();
        }
    }
}
