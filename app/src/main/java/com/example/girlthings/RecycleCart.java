package com.example.girlthings;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class RecycleCart extends AppCompatActivity {
    public static final String namaItem = "namaItem";
    public static final String hargaItem = "hargaItem";


    private RecyclerView mResultList;
    private DatabaseReference mDatabaseReference;
    FirebaseRecyclerAdapter adapter;
    int position;
    int total;
    String produk ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        mDatabaseReference = FirebaseDatabase.getInstance().getReference("Cart");
        TextView cek = findViewById(R.id.check);
        cek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toCheckout();
            }
        });
        mResultList =  findViewById(R.id.recyle);

        String searchText = "alex";
        firebaseUserView(searchText);
    }

    private void toCheckout() {
        String totalItem = produk;
        String totalHarga = String.valueOf(total);
        Intent intent = new Intent(this, totalpembelianbelipakaian.class);
        intent.putExtra(namaItem, totalItem);
        intent.putExtra(hargaItem, totalHarga);
        startActivity(intent);
    }

    private void firebaseUserView(String searchText) {

        Query query = mDatabaseReference.orderByChild("username").startAt(searchText).endAt(searchText + "\uf8ff");

        FirebaseRecyclerOptions<Cart> options =
                new FirebaseRecyclerOptions.Builder<Cart>()
                        .setQuery(query, Cart.class)
                        .build();
        adapter = new FirebaseRecyclerAdapter<Cart, CartViewHolder>(options) {
            @NonNull
            @Override
            public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_view_cart, parent, false);
                CartViewHolder viewHolder = new CartViewHolder(view);
                return viewHolder;
            }

            @Override
            protected void onBindViewHolder(@NonNull CartViewHolder holder, int i, @NonNull Cart model) {
                holder.cartBarang.setText(model.getBarang());
                holder.cartTotal.setText("Rp. "+model.getTotal());
                holder.cartJumlah.setText(model.getJumlah() + " Buah");
                String link = String.valueOf(model.getGambar());
                //Picasso.get().load(link).into(holder.cartGambar);

                String namaProduk = (String.valueOf(model.getBarang()));
                produk = produk + ", " + namaProduk;

                int price = (Integer.valueOf((model.getTotal())));
                total = total + price;

                holder.sampah.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String idData = getRef(position).getKey();
                        DatabaseReference delete = FirebaseDatabase.getInstance().getReference("Cart").child(idData);
                        delete.removeValue();
                        Toast.makeText(RecycleCart.this, "Delete Berhasil", Toast.LENGTH_LONG).show();
                    }
                });

            }
        };
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(this);
        mResultList.setLayoutManager(gridLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mResultList.getContext(),
                gridLayoutManager.getOrientation());
        mResultList.addItemDecoration(dividerItemDecoration);
        adapter.startListening();
        mResultList.setAdapter(adapter);
    }

    public static class CartViewHolder extends RecyclerView.ViewHolder {

        View mView;
        TextView cartBarang, cartTotal, cartJumlah;
        ImageView cartGambar, sampah;
        public CartViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            cartBarang = (TextView) mView.findViewById(R.id.namaBarang);
            cartTotal = (TextView) mView.findViewById(R.id.hargaBarang);
            cartJumlah = (TextView) mView.findViewById(R.id.jumlahBarang);
            cartGambar = (ImageView) mView.findViewById(R.id.imageBarang);
            sampah = mView.findViewById(R.id.trash);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(adapter!=null){
            adapter.startListening();
        }
    }

    @Override
    protected void onStop() {
        if(adapter!=null){
            adapter.stopListening();
        }
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(adapter!=null){
            adapter.startListening();
        }
    }
}
