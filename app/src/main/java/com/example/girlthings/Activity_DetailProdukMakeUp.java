package com.example.girlthings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.girlthings.model.UserModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Activity_DetailProdukMakeUp extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.android.twoactivities.extra.MESSAGE";
    public static final String EXTRA_MESSAGE_2 = "com.example.android.twoactivities.extra.MESSAGE_2";

    private FirebaseAuth mAuth;
    private FirebaseDatabase fDb;
    private DatabaseReference dbRef;
    private String uid;
    private UserModel userModel;


    Button beli,keranjang;
    EditText jumlah;
    int total, total1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_produk_make_up);

        jumlah = (EditText) findViewById(R.id.jumlahbarang);

        beli = (Button) findViewById(R.id.buttonbeli);
        beli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Pembayaran();
            }
        });

        mAuth = FirebaseAuth.getInstance();
        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference();
        mAuth = FirebaseAuth.getInstance();
        uid = mAuth.getUid();
        dbRef.child("users").child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userModel = dataSnapshot.getValue(UserModel.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Do Nothing
            }
        });

        keranjang = (Button)findViewById(R.id.buttonkeranjang);
        keranjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertToCart();
            }
        });
    }

    private void insertToCart() {
        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference mDbRef = mDatabase.getReference().child("Cart");

        String nama = "Concealer";
        String total = jumlah.getText().toString();
        String hargaPro = "75000";


        String numberOnly= hargaPro.replaceAll("[^0-9]", "");
        total1 = Integer.parseInt(total)*Integer.parseInt(numberOnly);

        String user = "alex";

        Cart cart = new Cart(userModel.getName(), nama, user, String.valueOf(total1), total);
        mDbRef.push().setValue(cart);

        Toast.makeText(Activity_DetailProdukMakeUp.this, "Berhasil Dimasukkan Keranjang", Toast.LENGTH_LONG).show();
    }

    public void Pembayaran(){
        String coba =  jumlah.getText().toString();
        String coba2 =  jumlah.getText().toString();
        Intent intent = new Intent(this, Activity_PembayaranMakeUp.class);
        intent.putExtra(EXTRA_MESSAGE, coba);
        intent.putExtra(EXTRA_MESSAGE_2, coba2);
        startActivity(intent);
    }

}
