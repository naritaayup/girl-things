package com.example.girlthings.model;

public class TransactionModel {
    private String userId;
    private int muaId;
    private String methodPayment;
    private String currentTime;
    private String totalPrice;

    public TransactionModel() {

    }

    public TransactionModel(String userId, int muaId, String methodPayment, String currentTime, String totalPrice) {
        this.userId = userId;
        this.muaId = muaId;
        this.methodPayment = methodPayment;
        this.currentTime = currentTime;
        this.totalPrice = totalPrice;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getMuaId() {
        return muaId;
    }

    public void setMuaId(int muaId) {
        this.muaId = muaId;
    }

    public String getMethodPayment() {
        return methodPayment;
    }

    public void setMethodPayment(String methodPayment) {
        this.methodPayment = methodPayment;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }
}
