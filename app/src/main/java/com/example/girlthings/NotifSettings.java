package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

public class NotifSettings extends AppCompatActivity {
    Spinner mSpNotifTransaksi;
    Button mBtn;
    ImageView mBtnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notif_settings);

        mBtnBack = (ImageView) findViewById(R.id.btnBack);
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent userProfile = new Intent(getApplicationContext(),UserProfile.class);
                startActivity(userProfile);
            }
        });

        mBtn = (Button) findViewById(R.id.btnSubmit);
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Selected ", Toast.LENGTH_SHORT).show();
            }
        });
    }
}