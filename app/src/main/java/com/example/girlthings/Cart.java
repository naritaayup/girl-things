package com.example.girlthings;

public class Cart {
    public String username, barang, gambar, total, jumlah;

    public Cart(String username, String barang, String gambar, String total, String jumlah) {
        this.username = username;
        this.barang = barang;
        this.gambar = gambar;
        this.total = total;
        this.jumlah = jumlah;
    }

    public Cart(){
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBarang() {
        return barang;
    }

    public void setBarang(String barang) {
        this.barang = barang;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }
}
