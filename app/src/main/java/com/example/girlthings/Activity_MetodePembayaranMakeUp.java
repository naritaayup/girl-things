package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.girlthings.Activity_DetailProdukMakeUp.EXTRA_MESSAGE;

public class Activity_MetodePembayaranMakeUp extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.android.twoactivities.extra.MESSAGE";
    public static final String EXTRA_MESSAGE_2 = "com.example.android.twoactivities.extra.MESSAGE_2";

    AppCompatRadioButton bankmandiri, bankbri, bankbca;

    String namabank;
    String pilihan;

    private TextView jumlahharga;
    Button BayarBarang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metode_pembayaran_make_up);

        jumlahharga = (TextView)findViewById(R.id.jumlahharga);

        BayarBarang = (Button) findViewById(R.id.buttonBayarBarang);
        BayarBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transferbarang();
            }
        });
        Intent intent = getIntent();
        String jumlah = intent.getStringExtra(EXTRA_MESSAGE);

        TextView textView = findViewById(R.id.jumlahharga);
        textView.setText(jumlah);

    }
    public void RadioButtonClicked(View view){
        boolean isSelected = ((AppCompatRadioButton)view).isSelected();
        switch (view.getId()){
            case R.id.bankmandiri:
                if (isSelected){
                    namabank = "Mandiri";
                }
                break;
            case R.id.bankbca:
                if (isSelected){
                    namabank = "BCA";
                }
                break;
            case R.id.bankbri:
                if (isSelected){
                    namabank = "BRI";
                }
                break;
        }
    }

    public void transferbarang(){
        if(namabank=="Mandiri"||namabank=="BankBCA"||namabank=="BankBRI"){
            String message2 = pilihan;
            String message = namabank;
            Intent intent = new Intent(this, Pembayaran.class);
            intent.putExtra(EXTRA_MESSAGE, message);
            intent.putExtra(EXTRA_MESSAGE_2, message2);
            startActivity(intent);
        } else {
            Toast.makeText(Activity_MetodePembayaranMakeUp.this, "Mohon Pilih Salah Satu Bank", Toast.LENGTH_LONG).show();
        }
    }
}
