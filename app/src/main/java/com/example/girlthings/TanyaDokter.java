package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class TanyaDokter extends AppCompatActivity {
    private Button mBtnTanya;
    private ImageView mBtnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tanya_dokter);

        mBtnBack = (ImageView) findViewById(R.id.btnBack);

        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent daftarDokter = new Intent(getApplicationContext(),DaftarDokter.class);
                startActivity(daftarDokter);
            }
        });

        mBtnTanya = (Button) findViewById(R.id.btnTanya);

        mBtnTanya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent alertSuccess = new Intent(getApplicationContext(), AskSuccess.class);
                startActivity(alertSuccess);
            }
        });

        final EditText name = (EditText) findViewById(R.id.txtPesan);
        mBtnTanya = (Button) findViewById(R.id.btnTanya);
        mBtnTanya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inbox = new Intent(TanyaDokter.this,Inbox.class);
                inbox.putExtra("name", name.getText().toString());
                startActivity(inbox);
            }
        });
    }
}
