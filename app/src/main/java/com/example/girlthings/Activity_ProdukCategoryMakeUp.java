package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Activity_ProdukCategoryMakeUp extends AppCompatActivity {

    private TextView category;

    private ImageView Powerstay;
    private ImageView Concealer;
    private ImageView NARS;
    private ImageView Fast;
    private ImageView BB;
    private ImageView Corrector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produk_category_make_up);

        //Text
        category = (TextView) findViewById(R.id.category);
        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Category();
            }
        });

        //Image
        Powerstay = (ImageView) findViewById(R.id.Powerstay);
        Powerstay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Detail();
            }
        });
        Concealer = (ImageView) findViewById(R.id.Concealer);
        Concealer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Detail();
            }
        });
        NARS = (ImageView) findViewById(R.id.NARS);
        NARS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Detail();
            }
        });
        Fast = (ImageView) findViewById(R.id.Fast2);
        Fast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Detail();
            }
        });
        BB = (ImageView) findViewById(R.id.BB2);
        BB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Detail();
            }
        });
        Corrector = (ImageView) findViewById(R.id.Corrector2);
        Corrector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Detail();
            }
        });

    }

    //Text
    public void Category(){
        Intent intent = new Intent(this, Activity_CategoryMakeUp.class);
        startActivity(intent);
    }

    //Image
    public void Detail(){
        Intent intent = new Intent(this, Activity_DetailProdukMakeUp.class);
        startActivity(intent);
    }

}
