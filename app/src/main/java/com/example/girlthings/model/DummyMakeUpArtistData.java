package com.example.girlthings.model;

import java.util.ArrayList;
import java.util.List;

public class DummyMakeUpArtistData {
    public static List<MakeUpArtistModel> makeUpArtistList() {
        MakeUpArtistModel wedding = new MakeUpArtistModel("Wedding",
                "Pernikahan adalah upacara pengikatan janji nikah yang " +
                        "dirayakan atau dilaksanakan oleh dua orang dengan maksud meresmikan " +
                        "ikatan perkawinan secara norma agama, norma hukum, dan norma sosial. " +
                        "Upacara pernikahan memiliki banyak ragam.",
                "https://www.hipwee.com/wp-content/uploads/2020/02/hipwee-page-63.jpg");
        MakeUpArtistModel graduation = new MakeUpArtistModel("Wisuda",
                "Upacara peneguhan atau pelantikan bagi seseorang yang telah menempuh " +
                        "pendidikan. Di kalangan akademik, wisuda merupakan penanda kelulusan " +
                        "mahasiswa yang telah menempuh masa belajar pada suatu universitas.",
                "https://kurio-img.kurioapps.com/18/02/03/78c95399d05a0e60c7d838ed1fb51091.jpeg");
        MakeUpArtistModel engagement = new MakeUpArtistModel("Engagement",
                "Merupakan sebutan untuk seseorang yang bertugas menentukan konsep " +
                        "berpakaian untuk klien supaya tampil memesona. Ya, profesi ini memang....",
                "https://i.ytimg.com/vi/LHPgehPgUn8/maxresdefault.jpg");

        List<MakeUpArtistModel> makeUpArtistModelList = new ArrayList<>();

        makeUpArtistModelList.add(wedding);
        makeUpArtistModelList.add(graduation);
        makeUpArtistModelList.add(engagement);

        return makeUpArtistModelList;
    }
}
