package com.example.girlthings.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.girlthings.Cart;
import com.example.girlthings.R;
import com.example.girlthings.model.UserModel;
import com.example.girlthings.totalpembelianbelipakaian;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;


public class CartFragment extends Fragment {
    private RecyclerView mResultList;
    private DatabaseReference mDatabaseReference;
    FirebaseRecyclerAdapter adapter;
    int position;
    int total;
    String produk ="";

    private FirebaseAuth mAuth;
    private FirebaseDatabase fDb;
    private DatabaseReference dbRef;
    private String uid;
    private UserModel userModel;

    public static final String namaItem = "namaItem";
    public static final String hargaItem = "hargaItem";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_cart, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDatabaseReference = FirebaseDatabase.getInstance().getReference("Cart");
        TextView cek = getActivity().findViewById(R.id.check);
        cek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toCheckout();
            }
        });
        mResultList =  getActivity().findViewById(R.id.recyle);

        mAuth = FirebaseAuth.getInstance();
        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference();
        mAuth = FirebaseAuth.getInstance();
        uid = mAuth.getUid();
        dbRef.child("users").child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userModel = dataSnapshot.getValue(UserModel.class);
                String searchText = userModel.getName();
                firebaseUserView(searchText);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Do Nothing
            }
        });


    }

    private void toCheckout() {
        String totalItem = produk;
        String totalHarga = String.valueOf(total);
        Intent intent = new Intent(getContext(), totalpembelianbelipakaian.class);
        intent.putExtra(namaItem, totalItem);
        intent.putExtra(hargaItem, totalHarga);
        startActivity(intent);
    }

    private void firebaseUserView(String searchText) {

        Query query = mDatabaseReference.orderByChild("username").startAt(searchText).endAt(searchText + "\uf8ff");

        FirebaseRecyclerOptions<Cart> options =
                new FirebaseRecyclerOptions.Builder<Cart>()
                        .setQuery(query, Cart.class)
                        .build();
        adapter = new FirebaseRecyclerAdapter<Cart, CartViewHolder>(options) {
            @NonNull
            @Override
            public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_view_cart, parent, false);
                CartViewHolder viewHolder = new CartViewHolder(view);
                return viewHolder;
            }

            @Override
            protected void onBindViewHolder(@NonNull CartViewHolder holder, int i, @NonNull Cart model) {
                holder.cartBarang.setText(model.getBarang());
                holder.cartTotal.setText("Rp. "+model.getTotal());
                holder.cartJumlah.setText(model.getJumlah() + " Buah");
                String link = String.valueOf(model.getGambar());
                Picasso.with(getContext()).load(link).into(holder.cartGambar);

                String namaProduk = (String.valueOf(model.getBarang()));
                produk = namaProduk + " ," +produk;

                int price = (Integer.valueOf((model.getTotal())));
                total = total + price;

                holder.sampah.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String idData = getRef(position).getKey();
                        DatabaseReference delete = FirebaseDatabase.getInstance().getReference("Cart").child(idData);
                        delete.removeValue();
                        Toast.makeText(getContext(), "Delete Berhasil", Toast.LENGTH_LONG).show();
                    }
                });
            }
        };

        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(getContext());
        mResultList.setLayoutManager(gridLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mResultList.getContext(),
                gridLayoutManager.getOrientation());
        mResultList.addItemDecoration(dividerItemDecoration);
        adapter.startListening();
        mResultList.setAdapter(adapter);
    }

    public static class CartViewHolder extends RecyclerView.ViewHolder {

        View mView;
        TextView cartBarang, cartTotal, cartJumlah;
        ImageView cartGambar, sampah;
        public CartViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            cartBarang = (TextView) mView.findViewById(R.id.namaBarang);
            cartTotal = (TextView) mView.findViewById(R.id.hargaBarang);
            cartJumlah = (TextView) mView.findViewById(R.id.jumlahBarang);
            cartGambar = (ImageView) mView.findViewById(R.id.imageBarang);
            sampah = mView.findViewById(R.id.trash);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if(adapter!=null){
            adapter.startListening();
        }
    }

    @Override
    public void onStop() {
        if(adapter!=null){
            adapter.stopListening();
        }
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(adapter!=null){
            adapter.startListening();
        }
    }
}
