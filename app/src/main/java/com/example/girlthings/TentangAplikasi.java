package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class TentangAplikasi extends AppCompatActivity {
    ImageView mBtnBack;
    TextView mTvLicenses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tentang_aplikasi);

        mBtnBack = (ImageView) findViewById(R.id.btnBack);
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent userProfile = new Intent(getApplicationContext(),UserProfile.class);
                startActivity(userProfile);
            }
        });

        mTvLicenses = (TextView) findViewById(R.id.tvLicenses);
        mTvLicenses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent licenses = new Intent(getApplicationContext(),Licenses.class);
                startActivity(licenses);
            }
        });
    }
}
