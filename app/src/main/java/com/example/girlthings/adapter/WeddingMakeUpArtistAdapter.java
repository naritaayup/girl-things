package com.example.girlthings.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.girlthings.DetailMuaActivity;
import com.example.girlthings.R;
import com.example.girlthings.model.ReviewModel;
import com.example.girlthings.model.WeddingMakeUpArtistModel;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class WeddingMakeUpArtistAdapter extends RecyclerView.Adapter<WeddingMakeUpArtistAdapter.WeddingMakeUpArtistViewHolder> implements Filterable {

    private Context context;
    private List<WeddingMakeUpArtistModel> weddingMakeUpArtistModels;
    private List<WeddingMakeUpArtistModel> weddingMakeUpArtistModelsFull;
    private List<ReviewModel> model = new ArrayList<>();
    private FirebaseDatabase fDb;
    private DatabaseReference dbRef;
    private String type;

    public WeddingMakeUpArtistAdapter(Context context, List<WeddingMakeUpArtistModel> weddingMakeUpArtistModels, String type) {
        this.context = context;
        this.weddingMakeUpArtistModels = weddingMakeUpArtistModels;
        weddingMakeUpArtistModelsFull = new ArrayList<>(weddingMakeUpArtistModels);
        this.type = type;
    }

    @Override
    public WeddingMakeUpArtistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wedding_make_up_artist, parent, false);
        return new WeddingMakeUpArtistViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final WeddingMakeUpArtistViewHolder holder, int position) {
        final WeddingMakeUpArtistModel item = weddingMakeUpArtistModels.get(position);
        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference();

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                model.clear();
                if (dataSnapshot.exists()) {
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        ReviewModel models = ds.getValue(ReviewModel.class);
                        model.add(models);
                    }

                    float sumRate = 0;
                    for (ReviewModel reviewModel : model) {
                        sumRate += Integer.valueOf(reviewModel.getRating());
                        Log.i("size", "size " + model.size());
                    }

                    float avgRate = sumRate / model.size();
                    holder.totalStar.setRating(avgRate);
                    holder.number_reviews.setText(model.size() + " reviews");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        Query query = dbRef.child("review").orderByChild("imageId").equalTo(item.id);
        query.addValueEventListener(valueEventListener);

        holder.textViewName.setText(item.name);
        holder.textViewDesc.setText(item.desc);
        Picasso.with(context).load(item.image).into(holder.imageView);

        holder.cardViewTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailMuaActivity.class);
                intent.putExtra("name", item.name);
                intent.putExtra("desc", item.desc);
                intent.putExtra("image", item.image);
                intent.putExtra("status", item.status);
                intent.putExtra("telp", item.telp);
                intent.putExtra("address", item.address);
                intent.putExtra("price", item.price);
                intent.putExtra("price", item.price);
                intent.putExtra("id", item.id);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return weddingMakeUpArtistModels.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<WeddingMakeUpArtistModel> item = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                item.addAll(weddingMakeUpArtistModelsFull);
            } else {
                String types = type.toLowerCase().trim();
                String charFilter = constraint.toString().toLowerCase().trim();

                for (WeddingMakeUpArtistModel items : weddingMakeUpArtistModelsFull) {
                    if (charFilter.toLowerCase().contains(types)) {
                        if (items.getType().contains(charFilter)) {
                            item.add(items);
                        }
                    } else {
                        if (items.getType().contains(types) && items.getPlace().contains(charFilter)) {
                            item.add(items);
                        }
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = item;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            weddingMakeUpArtistModels.clear();
            weddingMakeUpArtistModels.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    static class WeddingMakeUpArtistViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewDesc;
        ImageView imageView;
        CardView cardViewTwo;
        RatingBar totalStar;
        private TextView number_reviews;

        public WeddingMakeUpArtistViewHolder(View itemView) {
            super(itemView);

            cardViewTwo = itemView.findViewById(R.id.cardViewTwo);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewDesc = itemView.findViewById(R.id.textViewDesc);
            imageView = itemView.findViewById(R.id.imageViewMakeUpArtist);
            totalStar = itemView.findViewById(R.id.total_star);
            number_reviews = itemView.findViewById(R.id.number_reviews);
        }
    }


}
