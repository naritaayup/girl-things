package com.example.girlthings.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.girlthings.R;
import com.example.girlthings.WeddingMuaActivity;
import com.example.girlthings.model.MakeUpArtistModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MakeUpArtistAdapter extends RecyclerView.Adapter<MakeUpArtistAdapter.MakeUpArtistViewHolder> {

    private Context context;
    private List<MakeUpArtistModel> makeUpArtistModels;

    public MakeUpArtistAdapter(Context context, List<MakeUpArtistModel> makeUpArtistModels) {
        this.context = context;
        this.makeUpArtistModels = makeUpArtistModels;
    }

    @Override
    public MakeUpArtistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_make_up_artist, parent, false);
        return new MakeUpArtistViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MakeUpArtistViewHolder holder, int position) {
        final MakeUpArtistModel item = makeUpArtistModels.get(position);

        holder.textViewTitle.setText(item.title);
        holder.textViewDesc.setText(item.desc);
        Picasso.with(context).load(item.image).into(holder.imageView);

        holder.cardViewOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WeddingMuaActivity.class);
                intent.putExtra("wedding", item.title.toLowerCase());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return makeUpArtistModels.size();
    }

    static class MakeUpArtistViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle;
        TextView textViewDesc;
        ImageView imageView;
        CardView cardViewOne;

        public MakeUpArtistViewHolder(View itemView) {
            super(itemView);

            cardViewOne = itemView.findViewById(R.id.cardViewOne);
            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            textViewDesc = itemView.findViewById(R.id.textViewDesc);
            imageView = itemView.findViewById(R.id.imageViewMakeUpArtist);
        }
    }
}
