package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.example.girlthings.adapter.MakeUpArtistAdapter;
import com.example.girlthings.model.DummyMakeUpArtistData;
import com.example.girlthings.model.MakeUpArtistModel;

import java.util.List;

public class MakeUpArtistActivity extends AppCompatActivity {

    private RecyclerView recyclerViewMakeUpArtist;
    private MakeUpArtistAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_up_artist);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        recyclerViewMakeUpArtist = findViewById(R.id.recyclerViewMakeUpArtist);

        crateListData();
    }

    private void crateListData() {
        List<MakeUpArtistModel> dummy = DummyMakeUpArtistData.makeUpArtistList();
        recyclerViewMakeUpArtist.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MakeUpArtistAdapter(this, dummy);
        recyclerViewMakeUpArtist.setAdapter(adapter);
    }

    public void onBackPressed(View view) {
        finish();
    }
}
