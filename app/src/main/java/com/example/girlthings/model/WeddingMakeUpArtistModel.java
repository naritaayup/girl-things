package com.example.girlthings.model;

public class WeddingMakeUpArtistModel {

    public int id;
    public String name;
    public String desc;
    public String image;
    public String type;
    public String price;
    public String address;
    public String place;
    public String telp;
    public String status;

    public WeddingMakeUpArtistModel(int id, String name, String desc, String image, String place, String type, String price, String address, String telp, String status) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.image = image;
        this.place = place;
        this.type = type;
        this.price = price;
        this.address = address;
        this.telp = telp;
        this.status = status;
    }

    public WeddingMakeUpArtistModel() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
