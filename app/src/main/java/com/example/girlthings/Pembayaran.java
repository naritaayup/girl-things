package com.example.girlthings;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;

public class Pembayaran extends AppCompatActivity {
    public static final String EXTRA_MESSAGE =
            "com.example.android.twoactivities.extra.MESSAGE";
    public static final String EXTRA_MESSAGE_2 =
            "com.example.android.twoactivities.extra.MESSAGE_2";
    public static final String itemBarang = "";
    AppCompatRadioButton mandiri, bri, bca;
    TextView next;
    String bank;
    String hasil;
    String namaB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembayaran);
        next = (TextView) findViewById(R.id.Bayar);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toKonfPembayaran();
            }
        });
        Intent intent = getIntent();
        hasil = intent.getStringExtra(totalpembelianbelipakaian.EXTRA_MESSAGE);
        TextView biaya = findViewById(R.id.textView15);
        biaya.setText("Rp. "+hasil);

        namaB = intent.getStringExtra(totalpembelianbelipakaian.itemBarang);
    }


    public void radioButtonClicked(View view) {
        boolean isSelected = ((AppCompatRadioButton)view).isChecked();
        switch (view.getId()){
            case R.id.mandiri:
                if(isSelected){
                    bank = "Mandiri";
                }
                break;
            case R.id.bca:
                if(isSelected){
                    bank = "BCA";
                }
                break;
            case R.id.bri:
                if(isSelected){
                    bank = "BRI";
                }
                break;
        }
    }

    public void toKonfPembayaran() {
        if(bank=="Mandiri"||bank=="BCA"||bank=="BRI"){
            String message3 = namaB;
            String message2 = hasil;
            String message = bank;
            Intent intent = new Intent(this, Halamantransfer.class);
            intent.putExtra(EXTRA_MESSAGE, message);
            intent.putExtra(EXTRA_MESSAGE_2, message2);
            intent.putExtra(itemBarang, message3);
            startActivity(intent);
        }
        else {
            Toast.makeText(Pembayaran.this, "Mohon Pilih Salah Satu Bank", Toast.LENGTH_LONG).show();
        }
    }



}
