package com.example.girlthings;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

public class hasilSearch extends AppCompatActivity {

    public static final String nama_Barang = "nama";
    public static final String harga_Barang = "harga";
    public static final String deskripsi_Barang = "deskrpsi";
    public static final String image_Barang = "image";

    private RecyclerView mResultList;
    private EditText searchField;
    private ImageView searchImage;
    private DatabaseReference mPakaianDatabase;
    FirebaseRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_search);

        searchField = (EditText) findViewById(R.id.find);
        searchImage = (ImageView) findViewById(R.id.cari);

        mPakaianDatabase = FirebaseDatabase.getInstance().getReference("pakaian");

        mResultList = (RecyclerView) findViewById(R.id.result_list);

        searchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchText = searchField.getText().toString();
                firebaseUserSearch(searchText);
            }
        });

    }

    private void firebaseUserSearch(String searchText) {

        Toast.makeText(hasilSearch.this, "Started Search", Toast.LENGTH_LONG).show();

        Query query = mPakaianDatabase.orderByChild("nama").startAt(searchText).endAt(searchText + "\uf8ff");

        FirebaseRecyclerOptions<pakaian> options =
                new FirebaseRecyclerOptions.Builder<pakaian>()
                        .setQuery(query, pakaian.class)
                        .build();
        adapter = new FirebaseRecyclerAdapter<pakaian, UsersViewHolder>(options){
            @NonNull
            @Override
            public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_recycler_search, parent, false);
                UsersViewHolder viewHolder = new UsersViewHolder(view);
                return viewHolder;
            }
            @Override
            protected void onBindViewHolder(@NonNull final UsersViewHolder holder, int position, @NonNull final pakaian model) {
                holder.pakaian_nama.setText(model.getNama());
                holder.pakaian_desc.setText(model.getDeskripsi());
                holder.pakaian_harga.setText(model.getHarga());
                String link = String.valueOf(model.getGambar());
                //Picasso.get().load(link).into(holder.pakaian_gambar);
                Picasso.with(hasilSearch.this).load(link).into(holder.pakaian_gambar);

                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(hasilSearch.this, Halamandetailproduct.class);
                        String nama = model.getNama();
                        String harga = model.getHarga();
                        String deskripsi = model.getDeskripsi();
                        String gambar = model.getGambar();

                        intent.putExtra(nama_Barang, nama);
                        intent.putExtra(harga_Barang, harga);
                        intent.putExtra(deskripsi_Barang, deskripsi);
                        intent.putExtra(image_Barang, gambar);
                        startActivity(intent);

                    }
                });
            }
        };
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(this);
        mResultList.setLayoutManager(gridLayoutManager);
        adapter.startListening();
        mResultList.setAdapter(adapter);
    }

    public static class UsersViewHolder extends RecyclerView.ViewHolder {

        View mView;
        TextView pakaian_nama, pakaian_harga, pakaian_desc;
        ImageView pakaian_gambar;
        public UsersViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            pakaian_nama = (TextView) mView.findViewById(R.id.name);
            pakaian_harga = (TextView) mView.findViewById(R.id.price);
            pakaian_desc = (TextView) mView.findViewById(R.id.desc);
            pakaian_gambar = (ImageView) mView.findViewById(R.id.image);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(adapter!=null){
            adapter.startListening();
        }
    }

    @Override
    protected void onStop() {
        if(adapter!=null){
            adapter.stopListening();
        }
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(adapter!=null){
            adapter.startListening();
        }
    }
}
