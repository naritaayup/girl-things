package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class FashionDesigner extends AppCompatActivity {
    Button mBtnTanya;
    ImageView mBtnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fashion_designer);

        mBtnBack = (ImageView) findViewById(R.id.btnBack);
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent askExpert = new Intent(getApplicationContext(), AskExpert.class);
                startActivity(askExpert);
            }
        });

        mBtnTanya = (Button) findViewById(R.id.btnTanya);
        mBtnTanya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent daftarDesigner = new Intent(getApplicationContext(),DaftarDesigner.class);
                startActivity(daftarDesigner);
            }
        });
    }
}
