package com.example.girlthings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.example.girlthings.fragment.CartFragment;
import com.example.girlthings.fragment.HomeFragment;
import com.example.girlthings.fragment.InboxFragment;
import com.example.girlthings.fragment.ProfileFragment;
import com.example.girlthings.fragment.TransactionFragment;
import com.example.girlthings.model.UserModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.orhanobut.hawk.Hawk;

public class HomeActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseDatabase fDb;
    private DatabaseReference dbRef;
    private UserModel user;
    BottomNavigationView bottomNavigationView;

    private String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        Hawk.init(this).build();

        mAuth = FirebaseAuth.getInstance();
        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference("users");

        uid = mAuth.getUid();

        bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);

        if (mAuth.getCurrentUser() == null && mAuth.getUid() == null) {
            startActivity(new Intent(HomeActivity.this, masukogin.class));
        }

        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    user = dataSnapshot.child(uid).getValue(UserModel.class);
                    Hawk.put(Constant.USER, user.getName());
                    Hawk.put(Constant.PHONE, user.getPhoneNumber());
                    Hawk.put(Constant.EMAIL, user.getEmail());
                    Hawk.put(Constant.ADDRESS, user.getAddress());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Do Nothing
            }
        });

        Intent intent = getIntent();
        if (intent.hasExtra("action")) {
            if (intent.getStringExtra("action").equalsIgnoreCase("history")) {
                TransactionFragment fragmentTransaction = new TransactionFragment();
                FragmentTransaction fTTransaction = getSupportFragmentManager().beginTransaction();
                fTTransaction.replace(R.id.content, fragmentTransaction, "FragmentName");
                fTTransaction.commit();
            }
        } else {
            HomeFragment fragmentHome = new HomeFragment();
            FragmentTransaction fTHome = getSupportFragmentManager().beginTransaction();
            fTHome.replace(R.id.content, fragmentHome, "FragmentName");
            fTHome.commit();
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    HomeFragment fragmentHome = new HomeFragment();
                    FragmentTransaction fTHome = getSupportFragmentManager().beginTransaction();
                    fTHome.replace(R.id.content, fragmentHome, "FragmentName");
                    fTHome.commit();
                    return true;
                case R.id.navigation_inbox:
                    InboxFragment fragmentInbox = new InboxFragment();
                    FragmentTransaction fTInbox = getSupportFragmentManager().beginTransaction();
                    fTInbox.replace(R.id.content, fragmentInbox, "FragmentName");
                    fTInbox.commit();
                    return true;
                case R.id.navigation_cart:
                    CartFragment fragmentCart = new CartFragment();
                    FragmentTransaction fTCart = getSupportFragmentManager().beginTransaction();
                    fTCart.replace(R.id.content, fragmentCart, "FragmentName");
                    fTCart.commit();
                    return true;
                case R.id.navigation_transaction:
                    TransactionFragment fragmentTransaction = new TransactionFragment();
                    FragmentTransaction fTTransaction = getSupportFragmentManager().beginTransaction();
                    fTTransaction.replace(R.id.content, fragmentTransaction, "FragmentName");
                    fTTransaction.commit();
                    return true;
                case R.id.navigation_profile:
                    ProfileFragment fragmentProfile = new ProfileFragment();
                    FragmentTransaction fTProfile = getSupportFragmentManager().beginTransaction();
                    fTProfile.replace(R.id.content, fragmentProfile, "FragmentName");
                    fTProfile.commit();
                    return true;
            }
            return false;
        }
    };
}
