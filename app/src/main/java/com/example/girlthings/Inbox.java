package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Inbox extends AppCompatActivity {
    TextView mTvPesan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);
            mTvPesan = (TextView) findViewById(R.id.tvPesan);
            Bundle bundle=getIntent().getExtras();
            String pesan = bundle.getString("name");
            mTvPesan.setText(pesan);
    }
}

