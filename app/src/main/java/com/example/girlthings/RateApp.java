package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class RateApp extends AppCompatActivity {
    TextView mTvRate;
    Button mBtnSubmit;
    RatingBar mRatingBar;
    ImageView mBtnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_app);

            mTvRate = (TextView) findViewById(R.id.tvRate);
            mBtnSubmit = (Button) findViewById(R.id.btnSubmit);
            mRatingBar = (RatingBar) findViewById(R.id.ratingBar);

            mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float nilai, boolean b) {

                    mTvRate.setText("Rating: "+nilai);
                }
            });
            mBtnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Toast.makeText(getApplicationContext(), "Nilai Yang Anda Kirimkan: "+mRatingBar.getRating(), Toast.LENGTH_SHORT).show();
                }
            });

        mBtnBack = (ImageView) findViewById(R.id.btnBack);
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent userProfile = new Intent(getApplicationContext(), UserProfile.class);
                startActivity(userProfile);
            }
        });
        }
    }
