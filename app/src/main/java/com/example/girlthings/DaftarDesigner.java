package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class DaftarDesigner extends AppCompatActivity {
    private ImageView mBtnBack;
    private Button mBtnTanya;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_designer);

        mBtnBack = (ImageView) findViewById(R.id.btnBack);
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fashionDesigner = new Intent(getApplicationContext(),FashionDesigner.class);
                startActivity(fashionDesigner);
            }
        });

        mBtnTanya = (Button) findViewById(R.id.btnTanya);
        mBtnTanya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tanyaDesigner = new Intent(getApplicationContext(), Tanya.class);
                startActivity(tanyaDesigner);
            }
        });
    }
}
