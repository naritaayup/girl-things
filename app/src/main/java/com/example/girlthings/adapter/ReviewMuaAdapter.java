package com.example.girlthings.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.girlthings.R;
import com.example.girlthings.model.ReviewModel;

import java.util.ArrayList;
import java.util.List;

public class ReviewMuaAdapter extends RecyclerView.Adapter<ReviewMuaAdapter.ReviewMuaViewHolder> {

    private Context context;
    private List<ReviewModel> reviewModels = new ArrayList<>();

    public ReviewMuaAdapter(Context context){
        this.context = context;
    }

    public void setItems(List<ReviewModel> reviewModels){
        this.reviewModels.clear();
        this.reviewModels.addAll(reviewModels);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ReviewMuaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_review_mua, parent, false);
        return new ReviewMuaViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ReviewMuaViewHolder holder, final int position) {
        ReviewModel item = reviewModels.get(position);
        holder.labelName.setText(item.getUsername());
        holder.labelDesc.setText(item.getReview());
        holder.rate.setRating(Float.parseFloat(item.getRating()));
    }

    @Override
    public int getItemCount() {
        return reviewModels.size();
    }

    static class ReviewMuaViewHolder extends RecyclerView.ViewHolder {
        public TextView labelName, labelDesc;
        public ImageView labelPic;
        public RatingBar rate;

        public ReviewMuaViewHolder(@NonNull View view) {
            super(view);
            labelPic = view.findViewById(R.id.pic_user);
            labelName = view.findViewById(R.id.name);
            labelDesc = view.findViewById(R.id.comment);
            rate = view.findViewById(R.id.star);
        }
    }
}
