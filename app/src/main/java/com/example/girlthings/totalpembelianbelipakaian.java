package com.example.girlthings;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.girlthings.fragment.CartFragment;

public class totalpembelianbelipakaian extends AppCompatActivity {
    public static final String EXTRA_MESSAGE =
            "com.example.android.twoactivities.extra.MESSAGE";
    public static final String itemBarang =
            "itemBarang";
    private TextView next, totalharga;
    EditText address;
    Integer angka, total;
    String alamat;
    String namaItem = "";
    String hargaItem = "";
    String namaB = "Hijab Wardah Cream";
    String namaSkinB, hargaSkinB, jmlSkinB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_totalpembelianbelipakaian);
        next = (TextView) findViewById(R.id.textView13);
        totalharga = (TextView) findViewById(R.id.textView10);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toPembayaran();
            }
        });

        Intent intent = getIntent();
        Bundle bundle = getIntent().getExtras();

        if (intent.hasExtra("namaBarang")){
            String namaInt = bundle.getString("namaBarang");
            String hargaInt = bundle.getString("hargaBarang");
            String jumlah = bundle.getString("jmlBarang");
            String ukuran = bundle.getString("ukuranBarang");

            Toast.makeText(totalpembelianbelipakaian.this, namaInt + " " +hargaInt + " " +jumlah + " " +ukuran, Toast.LENGTH_LONG).show();

            TextView namab = findViewById(R.id.namaBa);
            namab.setText(namaInt+" "+ukuran);

            TextView hargab = findViewById(R.id.hargaitem);
            hargab.setText("Rp. "+hargaInt);

            TextView jmlb = findViewById(R.id.jumlahitem);
            jmlb.setText(jumlah);

            total = (Integer.parseInt(hargaInt)*Integer.parseInt(jumlah))+30000;

            TextView tot = findViewById(R.id.textView10);
            tot.setText("Rp. "+total);

            namaB = namaInt;
        }else if(intent.hasExtra(CartFragment.namaItem)){
            namaItem = intent.getStringExtra(RecycleCart.namaItem);
            hargaItem = intent.getStringExtra(RecycleCart.hargaItem);
            TextView textView4 = findViewById(R.id.namaBa);
            textView4.setText(namaItem);
            TextView textView6 = findViewById(R.id.hargaitem);
            textView6.setText("Rp. "+hargaItem);
            TextView textView8 = findViewById(R.id.jumlahitem);
            textView8.setText("");
            total = Integer.parseInt(hargaItem)+30000;
            totalharga.setText("Rp. "+total);
            namaB = namaItem;
        }else if(intent.hasExtra(beliSkincare.namaSkin)){
            namaSkinB = intent.getStringExtra(beliSkincare.namaSkin);
            hargaSkinB = intent.getStringExtra(beliSkincare.hargaSkin);
            jmlSkinB = intent.getStringExtra(beliSkincare.jmlSkin);

            TextView nmi = findViewById(R.id.namaBa);
            nmi.setText(namaSkinB);
            TextView hmi = findViewById(R.id.hargaitem);
            hmi.setText("Rp. "+hargaSkinB);
            TextView jmi = findViewById(R.id.jumlahitem);
            jmi.setText(jmlSkinB);

            total = (Integer.parseInt(hargaSkinB)*Integer.parseInt(jmlSkinB)) + 30000;
            TextView tot = findViewById(R.id.textView10);
            tot.setText("Rp. "+total);

            namaB = "Day Cream";
        };

        address = findViewById(R.id.editText3);
        alamat = address.getText().toString();

    }

    public void toPembayaran() {
        if (address.getText().toString().matches("")){
            Toast toast = Toast.makeText(getApplicationContext(), "Mohon isikan alamat", Toast.LENGTH_SHORT);
            toast.show();
        }else{
            String message = total.toString();
            String message2 = namaB;
            Intent intent = new Intent(this, Pembayaran.class);
            intent.putExtra(EXTRA_MESSAGE, message);
            intent.putExtra(itemBarang, message2);
            startActivity(intent);
        }
    }


    public void save(View view) {
        if (address.getText().toString().matches("")){
            Toast toast = Toast.makeText(getApplicationContext(), "Mohon isikan alamat", Toast.LENGTH_SHORT);
            toast.show();
        }else{
            Toast toast = Toast.makeText(getApplicationContext(), "Alamat Berhasil Disimpan", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
