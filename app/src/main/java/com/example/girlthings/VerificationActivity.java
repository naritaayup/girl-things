package com.example.girlthings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.mukesh.OtpView;

import java.util.concurrent.TimeUnit;

public class VerificationActivity extends AppCompatActivity {

    private TextView textViewCampaign;
    private OtpView otpView;
    private PhoneAuthProvider.ForceResendingToken token;
    private FirebaseAuth mAuth;

    private String name;
    private String phoneNumber;
    private String verificationId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        mAuth = FirebaseAuth.getInstance();
        name = getIntent().getStringExtra("name");
        phoneNumber = getIntent().getStringExtra("phoneNumber");

        textViewCampaign = findViewById(R.id.textViewCampaign);
        otpView = findViewById(R.id.otpView);

        String phone = "+62"+ phoneNumber.substring(1);
        textViewCampaign.setText(getString(R.string.verf_text)+ " " + phone);

        requestOtp(phone);
    }

    private void requestOtp(String phone) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(phone, 60L, TimeUnit.SECONDS, this, new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                verificationId = s;
                token = forceResendingToken;
            }

            @Override
            public void onCodeAutoRetrievalTimeOut(@NonNull String s) {
                super.onCodeAutoRetrievalTimeOut(s);
            }

            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {

            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                Toast.makeText(VerificationActivity.this, "Cannot create this account " + e.getMessage(), Toast.LENGTH_LONG).show();
                Log.d("test", "test" +  e.getMessage());
            }
        });
    }

    public void onBackPressed(View view) {
        finish();
    }

    public void loginIn(View view) {
        String otp = otpView.getText().toString();
        if (otp.length() == 6) {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, otp);
            veryfyAuth(credential);
        }
        startActivity(new Intent(this, HomeActivity.class));
    }

    private void veryfyAuth(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(VerificationActivity.this, "Authentication is successful", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(VerificationActivity.this, "Authentication is failed", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void resend(View view) {
    }
}
