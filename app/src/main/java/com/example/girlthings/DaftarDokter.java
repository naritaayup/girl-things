package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class DaftarDokter extends AppCompatActivity {
    private ImageView mBtnBack;
    private Button mBtnTanya;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_dokter);

        mBtnBack = (ImageView) findViewById(R.id.btnBack);
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent askExpert = new Intent(getApplicationContext(),DokterKecantikan.class);
                startActivity(askExpert);
            }
        });

        mBtnTanya = (Button) findViewById(R.id.btnTanya);
        mBtnTanya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tanyaDokter = new Intent(getApplicationContext(),TanyaDokter.class);
                startActivity(tanyaDokter);
            }
        });
    }
}
