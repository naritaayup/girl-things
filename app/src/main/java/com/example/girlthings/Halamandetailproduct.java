package com.example.girlthings;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;

import com.example.girlthings.model.UserModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class Halamandetailproduct extends AppCompatActivity {
    public static final String ukuranInt =
            "com.example.android.twoactivities.extra.MESSAGE";
    public static final String jmlInt =
            "com.example.android.twoactivities.extra.MESSAGE_2";
    public static final String hargaIntent =
            "com.example.android.twoactivities.extra.MESSAGE_2";
    public static final String namaIntent =
            "com.example.android.twoactivities.extra.MESSAGE_2";
    AppCompatRadioButton radioS, radioM, radioL, radioXL;
    String ukuran;
    Button beli;
    EditText jumlah;
    TextView produk, harga, deskr;
    int total1;

    private FirebaseAuth mAuth;
    private FirebaseDatabase fDb;
    private DatabaseReference dbRef;
    private String uid;
    private UserModel userModel;

    ImageView gambarB;
    String image = "https://firebasestorage.googleapis.com/v0/b/girls-things-46591.appspot.com/o/ic_background_creamfull.png?alt=media&token=afa81581-89aa-498f-80ff-995b3a64c9e9";

    private String takeGambar;
    private List<pakaian> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halamandetailproduct);
        produk = findViewById(R.id.namaProduk);
        beli = (Button) findViewById(R.id.button2);
        jumlah = (EditText) findViewById(R.id.editText);
        harga = findViewById(R.id.hargaP);
        deskr = findViewById(R.id.desc);
        gambarB = findViewById(R.id.gBarang);

        Intent intent = getIntent();
        if (intent.hasExtra(hasilSearch.nama_Barang)){
            String nama = intent.getStringExtra(hasilSearch.nama_Barang);
            String harga1 = intent.getStringExtra(hasilSearch.harga_Barang);
            String deskripsi = intent.getStringExtra(hasilSearch.deskripsi_Barang);
            image = intent.getStringExtra(hasilSearch.image_Barang);

            produk.setText(nama);
            harga.setText("Rp. "+ harga1);
            deskr.setText(deskripsi);
            Picasso.with(Halamandetailproduct.this).load(image).into(gambarB);
        }

        beli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toTotal();
            }
        });

        final TextView insertCart = findViewById(R.id.masukCart);
        insertCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertToCart();
            }
        });



        mAuth = FirebaseAuth.getInstance();
        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference();
        mAuth = FirebaseAuth.getInstance();
        uid = mAuth.getUid();
        dbRef.child("users").child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userModel = dataSnapshot.getValue(UserModel.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Do Nothing
            }
        });

//        ValueEventListener ambilGambar = new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                if(dataSnapshot.exists()){
//                    pakaian pakai = dataSnapshot.getValue(pakaian.class);
//                    takeGambar = pakai.getGambar();
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        };
//        Query query = dbRef.child("pakaian").orderByChild("nama").equalTo(produk.getText().toString());
//        query.addValueEventListener(ambilGambar);

        dbRef.child("pakaian").orderByChild("nama").equalTo(produk.getText().toString()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
//                    for (DataSnapshot ds: dataSnapshot.getChildren()){
//                        pakaian pakai = ds.getValue(pakaian.class);
//                        list.add(pakai);
//                    }
//                    takeGambar = list.getGambar();
            }}
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void insertToCart() {
        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference mDbRef = mDatabase.getReference().child("Cart");

        String name = produk.getText().toString();
        String total = jumlah.getText().toString();
        String hargaPro = harga.getText().toString();


        String numberOnly= hargaPro.replaceAll("[^0-9]", "");
        total1 = Integer.parseInt(total)*Integer.parseInt(numberOnly);


        Cart cart = new Cart(userModel.getName(), name, image, String.valueOf(total1), total);
        mDbRef.push().setValue(cart);

        Toast.makeText(Halamandetailproduct.this, "Berhasil Dimasukkan Keranjang", Toast.LENGTH_LONG).show();
    }

    public void radioButtonClicked(View view) {
        boolean isSelected = ((AppCompatRadioButton)view).isChecked();
        switch (view.getId()){
            case R.id.radioS:
                if(isSelected){
                    ukuran = "S";
                }
                break;
            case R.id.radioM:
                if(isSelected){
                    ukuran = "M";
                }
                break;
            case R.id.radioL:
                if(isSelected){
                    ukuran = "L";
                }
                break;
            case R.id.radioXL:
                if(isSelected){
                    ukuran = "XL";
                }
                break;
        }
    }

    public void toTotal() {
        String namaBarang = produk.getText().toString();
        String coba4 = harga.getText().toString();
        String hargaBarang = coba4.replaceAll("[^0-9]", "");
        String ukuranBarang = ukuran;
        String jmlBarang = jumlah.getText().toString();

        Bundle bundle = new Bundle();
        bundle.putString("namaBarang", namaBarang);
        bundle.putString("hargaBarang", hargaBarang);
        bundle.putString("ukuranBarang", ukuranBarang);
        bundle.putString("jmlBarang", jmlBarang);

        Intent intent = new Intent(this, totalpembelianbelipakaian.class);
        intent.putExtras(bundle);
//        intent.putExtra(ukuranInt, ukuran);
//        intent.putExtra(jmlInt, jumlah.getText().toString());
//        intent.putExtra(namaIntent, produk.getText().toString());
//        intent.putExtra(hargaIntent, hargaBarang);
        startActivity(intent);
//        Toast.makeText(Halamandetailproduct.this, namaBarang+" "+hargaBarang+" "+ukuranBarang+" "+jmlBarang, Toast.LENGTH_LONG).show();
    }
}
