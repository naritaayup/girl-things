package com.example.girlthings;

import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class KonfirmasiPembayaranMakeUp {

    String nama = "Muhammad Zikri";
    String bank ="Bank Mandiri";
    String jumlah = "930000";

    @Rule
    public ActivityTestRule<Activity_KonfirmasiPembayaranMakeUp> activityRule = new ActivityTestRule<Activity_KonfirmasiPembayaranMakeUp>(Activity_KonfirmasiPembayaranMakeUp.class);

    @Test
    public void isinilai() throws Exception {
        onView(withId(R.id.editText6)).perform(typeText(nama), closeSoftKeyboard());
        onView(withId(R.id.editText7)).perform(typeText(bank), closeSoftKeyboard());
        onView(withId(R.id.editText8)).perform(typeText(jumlah), closeSoftKeyboard());
        onView(withId(R.id.Kirim)).perform(click());
    }
}
