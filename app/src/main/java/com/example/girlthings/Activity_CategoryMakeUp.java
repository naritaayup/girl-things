package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Activity_CategoryMakeUp extends AppCompatActivity {

    private ImageView Powerstay2;
    private ImageView concealer2;
    private ImageView NARS2;
    private ImageView Fast2;
    private ImageView BB2;
    private ImageView Corrector2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_make_up);

        Powerstay2 = (ImageView)findViewById(R.id.Powerstay2);
        Powerstay2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Detail();
            }
        });
        concealer2 = (ImageView)findViewById(R.id.concealer2);
        concealer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Detail();
            }
        });
        NARS2 = (ImageView)findViewById(R.id.Nars2);
        NARS2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Detail();
            }
        });
        Fast2 = (ImageView)findViewById(R.id.Fast2);
        Fast2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Detail();
            }
        });
        BB2 = (ImageView)findViewById(R.id.BB2);
        BB2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Detail();
            }
        });
        Corrector2 = (ImageView)findViewById(R.id.Corrector2);
        Corrector2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Detail();
            }
        });

    }

    public void Detail(){
        Intent intent = new Intent(this, Activity_DetailProdukMakeUp.class);
        startActivity(intent);
    }

}
