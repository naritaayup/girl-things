package com.example.girlthings;

public class transaksiStatus {
    String idTra, barang, status, total, username, image;

    public transaksiStatus(String idTra, String barang, String status, String total, String username, String image) {
        this.idTra = idTra;
        this.barang = barang;
        this.status = status;
        this.total = total;
        this.username = username;
        this.image = image;
    }

    public transaksiStatus() {
    }

    public String getIdTra() {
        return idTra;
    }

    public void setIdTra(String idTra) {
        this.idTra = idTra;
    }

    public String getBarang() {
        return barang;
    }

    public void setBarang(String barang) {
        this.barang = barang;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
