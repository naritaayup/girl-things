package com.example.girlthings.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.girlthings.Activity_MakeUp;
import com.example.girlthings.AskExpert;
import com.example.girlthings.BeliPakaian1;
import com.example.girlthings.MakeUpArtistActivity;
import com.example.girlthings.R;
import com.example.girlthings.adapter.DashboardGridViewAdapter;
import com.example.girlthings.adapter.ViewPagerAdapter;
import com.example.girlthings.halamanSkincare;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class HomeFragment extends Fragment {

    private ViewPagerAdapter viewPagerAdapter;
    private ViewPager viewPager;
    private List<String> slider = new ArrayList<>();
    private ArrayList<String> listImage;
    private ArrayList<Integer> listText;
    private TabLayout tabLayout;
    private DashboardGridViewAdapter mAdapter;
    private GridView gridView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        showSlider();
        createGridView();
    }

    private void createGridView() {
        listImage = new ArrayList<String>();
        listImage.add(getString(R.string.pakaian));
        listImage.add(getString(R.string.skin_care));
        listImage.add(getString(R.string.make_up));
        listImage.add(getString(R.string.ask_ekspert));
        listImage.add(getString(R.string.make_up_artist));

        listText = new ArrayList<Integer>();
        listText.add(R.drawable.tshirt);
        listText.add(R.drawable.skincare);
        listText.add(R.drawable.makeup);
        listText.add(R.drawable.ask);
        listText.add(R.drawable.makeupartist);

        mAdapter = new DashboardGridViewAdapter(getContext(), listImage, listText);
        gridView = (GridView) getActivity().findViewById(R.id.dashboardGrid);
        gridView.setAdapter(mAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    startActivity(new Intent(getContext(), BeliPakaian1.class));
                } else if (position == 1) {
                    startActivity(new Intent(getContext(), halamanSkincare.class));
                } else if (position == 2) {
                    startActivity(new Intent(getContext(), Activity_MakeUp.class));
                } else if (position == 3) {
                    startActivity(new Intent(getContext(), AskExpert.class));
                } else if (position == 4) {
                    startActivity(new Intent(getContext(), MakeUpArtistActivity.class));
                }
            }
        });
    }

    private void showSlider() {
        slider.add("https://cdn2.tstatic.net/kaltim/foto/bank/images/promo-guardian.jpg");
        slider.add("https://cf.shopee.co.id/file/55250dc317f63f9b4f6f96915b2980d3");

        viewPagerAdapter = new ViewPagerAdapter(getActivity(), slider);
        viewPager = getActivity().findViewById(R.id.slider);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout = getActivity().findViewById(R.id.viewPagerIndicator);
        tabLayout.setupWithViewPager(viewPager, true);
        viewPager.setCurrentItem(slider.size() - 1);

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (getActivity() == null)
                    return;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (viewPager.getCurrentItem() < slider.size() - 1) {
                            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                        } else {
                            viewPager.setCurrentItem(0);
                        }
                    }
                });
            }
        }, 4000, 6000);
    }
}
