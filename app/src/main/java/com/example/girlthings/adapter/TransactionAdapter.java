package com.example.girlthings.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.girlthings.ConfirmationPaymentActivity;
import com.example.girlthings.Halamankonfirmasibayar;
import com.example.girlthings.R;
import com.example.girlthings.model.ConfirmationModel;
import com.example.girlthings.model.ReviewModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder> {

    private Context context;
    private List<ConfirmationModel> confirmationModel;
    private Dialog dialog;
    private FirebaseAuth mAuth;
    private FirebaseDatabase fDb;
    private DatabaseReference dbRef;
    private long reviewId;
    private String uid;
    private int id;
    private ReviewModel reviewModel;
    private String username;

    public static final String EXTRA_MESSAGE_2 =
            "com.example.android.twoactivities.extra.MESSAGE_2";

    public TransactionAdapter(Context context, List<ConfirmationModel> confirmationModel) {
        this.context = context;
        this.confirmationModel = confirmationModel;
    }

    @NonNull
    @Override
    public TransactionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_view_transaksi, parent, false);
        return new TransactionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionViewHolder holder, final int position) {
        mAuth = FirebaseAuth.getInstance();
        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference();
        final ConfirmationModel item = confirmationModel.get(position);

        uid = mAuth.getUid();

        dbRef.child("review").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                reviewId = dataSnapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        Log.i("position", "position " + position);
        holder.traId.setText("ID : " + item.getIdTra());
        holder.traBarang.setText(item.getBarang());
        holder.traTotal.setText("Rp." + item.getTotal());
        if (String.valueOf(item.getStatus()).matches("Belum Dibayar")) {
            holder.confirmation.setVisibility(View.VISIBLE);
            holder.review.setVisibility(View.GONE);
            if (item.getBarang().equalsIgnoreCase("Make Up Artist")) {
                holder.confirmation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ConfirmationPaymentActivity.class);
                        intent.putExtra("username", item.getUsername());
                        intent.putExtra("price", item.getTotal());
                        intent.putExtra("transId", item.getIdTra());
                        intent.putExtra("confsId", position+1);
                        context.startActivity(intent);
                    }
                });
            } else {
                holder.confirmation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, Halamankonfirmasibayar.class);
                        intent.putExtra("username", item.getUsername());
                        intent.putExtra(EXTRA_MESSAGE_2, item.getTotal());
                        intent.putExtra("transId", item.getIdTra());
                        intent.putExtra("ids", position+1);
                        context.startActivity(intent);
                    }
                });
            }
        } else if (String.valueOf(item.getStatus()).matches("Pending")) {
            holder.traStatus.setBackgroundResource(R.drawable.statusorange);
            holder.confirmation.setVisibility(View.GONE);
            holder.review.setVisibility(View.GONE);
        } else if (String.valueOf(item.getStatus()).matches("Dibayar")) {
            holder.traStatus.setBackgroundResource(R.drawable.statushijau);
            holder.confirmation.setVisibility(View.GONE);
            if (item.getBarang().equalsIgnoreCase("Make Up Artist")) {
                holder.review.setVisibility(View.VISIBLE);
                id = item.getIdTra();
                username = item.getUsername();
                holder.review.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        messageDialog();
                    }
                });
            }
        } else if (String.valueOf(item.getStatus()).matches("Dikirim")) {
            holder.traStatus.setBackgroundResource(R.drawable.statusbiru);
            holder.confirmation.setVisibility(View.GONE);
            if (item.getBarang().equalsIgnoreCase("Make Up Artist")) {
                holder.review.setVisibility(View.VISIBLE);
                holder.review.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        messageDialog();
                    }
                });
            }
        }
        holder.traStatus.setText(item.getStatus());
    }

    private void messageDialog() {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_review);
        final RatingBar valueRating = dialog.findViewById(R.id.value_ratting);
        final EditText valueReview = dialog.findViewById(R.id.value_review);
        Button btnReview = dialog.findViewById(R.id.btn_review);

        btnReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int intValueRating = (int) valueRating.getRating();
                String review = valueReview.getText().toString();
                int ids = (int) reviewId + 1;
                reviewModel = new ReviewModel(uid, String.valueOf(intValueRating), review, username, "", id);
                dbRef.child("review").child(ids + "").setValue(reviewModel);
                dialog.dismiss();
            }
        });

        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    @Override
    public int getItemCount() {
        return confirmationModel.size();
    }

    static class TransactionViewHolder extends RecyclerView.ViewHolder {

        TextView traBarang, traTotal, traId, traStatus, review, confirmation;

        public TransactionViewHolder(@NonNull View itemView) {
            super(itemView);
            traId = (TextView) itemView.findViewById(R.id.idTra);
            traBarang = (TextView) itemView.findViewById(R.id.namaTransaksi);
            traTotal = (TextView) itemView.findViewById(R.id.totalTransaksi);
            traStatus = (TextView) itemView.findViewById(R.id.statusTra);
            review = (TextView) itemView.findViewById(R.id.ReviewTra);
            confirmation = (TextView) itemView.findViewById(R.id.confirmPayment);
        }
    }
}
