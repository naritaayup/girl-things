package com.example.girlthings;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.girlthings.model.ConfirmationModel;
import com.example.girlthings.model.UserModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Halamantransfer extends AppCompatActivity {
    public static final String EXTRA_MESSAGE =
            "com.example.android.twoactivities.extra.MESSAGE";
    public static final String EXTRA_MESSAGE_2 =
            "com.example.android.twoactivities.extra.MESSAGE_2";
    public static final String itemBarang = "";

    TextView next;
    String harga, bank, namaB;

    private FirebaseAuth mAuth;
    private FirebaseDatabase fDb;
    private DatabaseReference dbRef;
    private UserModel userModel;
    private ConfirmationModel consModel;

    private String uid;
    private long transId;
    private long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halamantransfer);

        mAuth = FirebaseAuth.getInstance();
        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference();

        uid = mAuth.getUid();

        next = (TextView) findViewById(R.id.textView31);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toKonfirmasi();
            }
        });
        Intent intent = getIntent();
        bank = intent.getStringExtra(Pembayaran.EXTRA_MESSAGE);
        harga = intent.getStringExtra(Pembayaran.EXTRA_MESSAGE_2);
        TextView namabank = findViewById(R.id.search3);
        TextView total = findViewById(R.id.textView30);
        TextView norek = findViewById(R.id.textView21);
        namabank.setText("Transfer "+bank);
        total.setText("Rp. "+harga);
        if (bank.matches("Mandiri")){
            norek.setText("147-1000-2456-1231");
        }else if (bank.matches("BCA")){
            norek.setText("256-2000-421-9987");
        }else{
            norek.setText("678-4000-1294-4215");
        }

        dbRef.child("users").child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userModel = dataSnapshot.getValue(UserModel.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Do Nothing
            }
        });

        dbRef.child("transaksiStatus").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                id = dataSnapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        namaB = intent.getStringExtra(Pembayaran.itemBarang);


    }
    public void toKonfirmasi() {
        String message = bank;
        String message2 = harga;
        String message3 = namaB;
        final int transId = (int) (Math.random()*10000)/3;
        final int ids = (int) id+1;

        consModel = new ConfirmationModel(transId, "Belum Bayar", "", userModel.getName(), message2, message3);
        dbRef.child("transaksiStatus").child(ids + "").setValue(consModel);

        Intent intent = new Intent(this, Halamankonfirmasibayar.class);
        intent.putExtra("ids", ids);
        intent.putExtra("transId", transId);
        intent.putExtra("username", userModel.getName());
        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra(EXTRA_MESSAGE_2, message2);
        intent.putExtra(itemBarang, message3);
        startActivity(intent);
    }
}
