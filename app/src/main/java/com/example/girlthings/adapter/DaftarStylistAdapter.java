package com.example.girlthings.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.girlthings.DaftarStylist;
import com.example.girlthings.R;
import com.example.girlthings.Tanya;
import com.example.girlthings.model.DaftarStylistModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DaftarStylistAdapter extends RecyclerView.Adapter<DaftarStylistAdapter.DaftarStylistViewHolder>{
    private Context context;
    private List<DaftarStylistModel> list;

    public DaftarStylistAdapter(Context context, List<DaftarStylistModel> list){
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public DaftarStylistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_daftar_stylist, parent, false);
        return new DaftarStylistViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DaftarStylistViewHolder holder, int position) {
        final DaftarStylistModel item = list.get(position);

        Picasso.with(context).load(item.image).into(holder.imgStylist);
        holder.tvNamaStylist.setText(item.nama);
        holder.tvAsalStylist.setText(item.asal);
        holder.btnTanya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tanya = new Intent(context, Tanya.class);
                tanya.putExtra("nama", item.nama);
                tanya.putExtra("asal", item.asal);
                context.startActivity(tanya);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class DaftarStylistViewHolder extends RecyclerView.ViewHolder {

        ImageView imgStylist;
        TextView  tvNamaStylist;
        TextView tvAsalStylist;
        Button btnTanya;


        public DaftarStylistViewHolder(View itemView) {
            super(itemView);

            imgStylist = itemView.findViewById(R.id.imgStylist);
            tvNamaStylist = itemView.findViewById(R.id.tvNamaStylist);
            tvAsalStylist = itemView.findViewById(R.id.tvAsalStylist);
            btnTanya = itemView.findViewById(R.id.btnTanya);
        }
    }
}
