package com.example.girlthings;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class BeliPakaian1 extends AppCompatActivity {
    private EditText search;
    private ImageView hijab;
    private ImageView cari;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beli_pakaian1);
        search = findViewById(R.id.find);
        hijab = (ImageView) findViewById(R.id.hijab);
        hijab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toHijab();
            }
        });

        cari = (ImageView) findViewById(R.id.imageView);
        cari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toSearch();
            }
        });

//        ImageView tes = findViewById(R.id.tes);
//        tes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                toCart();
//            }
//        });

//        ImageView pa = findViewById(R.id.pajama);
//        pa.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                toStatus();
//            }
//        });

//        ImageView jin = findViewById(R.id.jeans);
//        jin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                DarkMode();
//            }
//        });

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("message");

        myRef.setValue("Hello, World!");
    }

//    private void DarkMode() {
//
//        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
//            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
//        } else {
//            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
//        }
//
//        finish();
//        startActivity(new Intent(MainActivity.this, MainActivity.this.getClass()));
//    }

//    private void toStatus() {
//        Intent intent = new Intent(this, recyclerTransaksi.class);
//        startActivity(intent);
//    }

//    private void toCart() {
//        Intent intent = new Intent(this, RecycleCart.class);
//        startActivity(intent);
//    }

    public void toHijab() {
        Intent intent = new Intent(this, Belipakaianhijab.class);
        startActivity(intent);
    }

    public void toSearch() {
        Intent intent = new Intent(this, hasilSearch.class);
        startActivity(intent);
    }
}
