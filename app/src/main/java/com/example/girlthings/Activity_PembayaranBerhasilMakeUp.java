package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Activity_PembayaranBerhasilMakeUp extends AppCompatActivity {

    private ImageView back9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembayaran_berhasil_make_up);

        back9 = (ImageView) findViewById(R.id.back9);
        back9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });
    }
    public void back(){
        Intent intent = new Intent(this, Activity_KonfirmasiPembayaranMakeUp.class);
        startActivity(intent);
    }
}
