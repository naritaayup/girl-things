package com.example.girlthings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.girlthings.model.WeddingMakeUpArtistModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AddDataDummy extends AppCompatActivity {

    public EditText name, desc, price, address, place, telp, type;
    Button save;
    long id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_data_dummy);

        name = findViewById(R.id.name);
        desc = findViewById(R.id.desc);
        price = findViewById(R.id.price);
        address = findViewById(R.id.address);
        place = findViewById(R.id.place);
        telp = findViewById(R.id.telp);
        type = findViewById(R.id.type);
        save = findViewById(R.id.save);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference dbRef = database.getReference("mua");

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dbRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        id = dataSnapshot.getChildrenCount();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                int ids = (int) id + 1;
                String names = name.getText().toString();
                String descs = desc.getText().toString();
                String types = type.getText().toString();
                String prices = price.getText().toString();
                String addresss = address.getText().toString();
                String places = place.getText().toString();
                String telps = telp.getText().toString();

                WeddingMakeUpArtistModel model = new WeddingMakeUpArtistModel(
                        ids,
                        names,
                        descs,
                        "test",
                        places,
                        types,
                        prices,
                        addresss,
                        telps,
                        "open");

                dbRef.child(ids+"").setValue(model);
            }
        });
    }
}
