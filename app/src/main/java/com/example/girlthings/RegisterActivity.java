package com.example.girlthings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.girlthings.model.UserModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {

    private EditText editTextName;
    private EditText editTextUserName;
    private EditText editTextEmail;
    private EditText editTextPhoneNumber;
    private EditText editTextPass;

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    private String fullName;
    private String userName;
    private String email;
    private String phoneNumber;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        if (mAuth.getCurrentUser() != null && mAuth.getUid() != null) {
            startActivity(new Intent(RegisterActivity.this, HomeActivity.class));
        }

        editTextName = findViewById(R.id.editTextFullName);
        editTextUserName = findViewById(R.id.editTextUserName);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPhoneNumber = findViewById(R.id.editTextPhoneNumber);
        editTextPass = findViewById(R.id.editTextPass);
    }

    public void signUp(View view) {
        if (!validateForm()) {
            return;
        } else {
            fullName = editTextName.getText().toString();
            userName = editTextUserName.getText().toString();
            email = editTextEmail.getText().toString();
            phoneNumber = editTextPhoneNumber.getText().toString();
            password = editTextPass.getText().toString();

            mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(
                    this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                onAuthSuccess(task.getResult().getUser(), fullName, userName, phoneNumber);
                                Toast.makeText(RegisterActivity.this, "Sign Up Success ", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(RegisterActivity.this, "Sign Up Failed " + task.getException(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
            );
        }
    }

    private boolean validateForm() {
        boolean valid = true;

        fullName = editTextName.getText().toString();
        if (TextUtils.isEmpty(fullName)) {
            editTextName.setError(getString(R.string.fill_cannot_empty));
            valid = false;
        } else {
            editTextName.setError(null);
        }

        userName = editTextUserName.getText().toString();
        if (TextUtils.isEmpty(userName)) {
            editTextUserName.setError(getString(R.string.fill_cannot_empty));
            valid = false;
        } else {
            editTextEmail.setError(null);
        }

        email = editTextEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            editTextEmail.setError(getString(R.string.fill_cannot_empty));
            valid = false;
        } else {
            editTextEmail.setError(null);
        }

        phoneNumber = editTextPhoneNumber.getText().toString();
        if (TextUtils.isEmpty(phoneNumber)) {
            editTextPhoneNumber.setError(getString(R.string.fill_cannot_empty));
            valid = false;
        } else {
            editTextPhoneNumber.setError(null);
        }

        password = editTextPass.getText().toString();
        if (TextUtils.isEmpty(password)) {
            editTextPass.setError(getString(R.string.fill_cannot_empty));
            valid = false;
        } else {
            editTextPass.setError(null);
        }

        return valid;
    }

    private void onAuthSuccess(FirebaseUser user, String fullName, String userName, String phoneNumber) {
        saveNewUser(user.getUid(), user.getEmail(), fullName, userName, phoneNumber);

        startActivity(new Intent(RegisterActivity.this, masukogin.class));
        finish();
    }

    private void saveNewUser(String uid, String email, String fullName, String userName, String phoneNumber) {
        UserModel user = new UserModel(fullName, userName, email, phoneNumber, "", "");
        mDatabase.child("users").child(uid).setValue(user);
    }

    public void onBackPressed(View view) {
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mAuth.getCurrentUser() != null && mAuth.getUid() != null) {
            startActivity(new Intent(RegisterActivity.this, HomeActivity.class));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAuth.getCurrentUser() != null && mAuth.getUid() != null) {
            startActivity(new Intent(RegisterActivity.this, HomeActivity.class));
        }
    }
}
