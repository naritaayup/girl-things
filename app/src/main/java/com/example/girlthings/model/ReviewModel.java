package com.example.girlthings.model;

public class ReviewModel {
    private String userId;
    private String rating;
    private String review;
    private String username;
    private int imageId;
    private String image;

    public ReviewModel() {

    }

    public ReviewModel(String userId, String rating, String review, String username, String image, int imageId) {
        this.userId = userId;
        this.rating = rating;
        this.review = review;
        this.username = username;
        this.image = image;
        this.imageId = imageId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
