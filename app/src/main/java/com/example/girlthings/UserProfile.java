package com.example.girlthings;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class UserProfile extends AppCompatActivity {
    private TextView mTvNotif, mTvEdit, mTvRate, mTvTentang, mTvSyarat, mTvKebijakan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        mTvNotif = (TextView) findViewById(R.id.tvNotif);
        mTvNotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent notifSettings = new Intent(getApplicationContext(), NotifSettings.class);
                startActivity(notifSettings);
            }
        });

        mTvEdit = (TextView) findViewById(R.id.tvEdit);
        mTvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editProfile = new Intent(getApplicationContext(), EditProfile.class);
                startActivity(editProfile);
            }
        });

        mTvRate = (TextView) findViewById(R.id.tvRate);
        mTvRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent rateApp = new Intent(getApplicationContext(), RateApp.class);
                startActivity(rateApp);
            }
        });

        mTvTentang = (TextView) findViewById(R.id.tvTentang);
        mTvTentang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tentangAplikasi = new Intent(getApplicationContext(), TentangAplikasi.class);
                startActivity(tentangAplikasi);
            }
        });

        mTvSyarat = (TextView) findViewById(R.id.tvSyarat);
        mTvSyarat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent syaratKetentuan = new Intent(getApplicationContext(), SyaratKetentuan.class);
                startActivity(syaratKetentuan);
            }
        });

        mTvKebijakan = (TextView) findViewById(R.id.tvKebijakan);
        mTvKebijakan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent kebijakanPrivasi = new Intent(getApplicationContext(), KebijakanPrivasi.class);
                startActivity(kebijakanPrivasi);
            }
        });

    }
}