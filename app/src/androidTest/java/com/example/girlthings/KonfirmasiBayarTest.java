package com.example.girlthings;


import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isChecked;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class KonfirmasiBayarTest {
    @Rule
    public ActivityTestRule<Pembayaran> activityRule =
            new ActivityTestRule<Pembayaran>(Pembayaran.class);

    @Test
    public void checkMandiri() throws Exception {
        onView(withId(R.id.mandiri))
                .perform(click());

        onView(withId(R.id.mandiri))
                .check(matches(isChecked()));

        onView(withId(R.id.bri))
                .check(matches(not(isChecked())));

        onView(withId(R.id.bca))
                .check(matches(not(isChecked())));
        onView(withId(R.id.Bayar)).perform(click());
    }

    @Test
    public void checkBRI() throws Exception {
        onView(withId(R.id.bri))
                .perform(click());

        onView(withId(R.id.mandiri))
                .check(matches(not(isChecked())));

        onView(withId(R.id.bri))
                .check(matches(isChecked()));

        onView(withId(R.id.bca))
                .check(matches(not(isChecked())));
        onView(withId(R.id.Bayar)).perform(click());
    }

    @Test
    public void checkBCA() throws Exception {
        onView(withId(R.id.bca))
                .perform(click());

        onView(withId(R.id.mandiri))
                .check(matches(not(isChecked())));

        onView(withId(R.id.bri))
                .check(matches(not(isChecked())));

        onView(withId(R.id.bca))
                .check(matches(isChecked()));
        onView(withId(R.id.Bayar)).perform(click());
    }

    @Test
    public void noCheck() throws Exception {
        onView(withId(R.id.Bayar)).perform(click());
    }


}
