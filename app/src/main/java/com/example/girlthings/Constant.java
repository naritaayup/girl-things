package com.example.girlthings;

public class Constant {

    public static final String USER = "username";
    public static final String PHONE = "phone";
    public static final String EMAIL = "email";
    public static final String ADDRESS = "address";
}
