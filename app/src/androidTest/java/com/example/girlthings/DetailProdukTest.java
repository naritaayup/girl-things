package com.example.girlthings;

import android.app.Activity;

import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class DetailProdukTest {

    String nilai = "10";

    @Rule
    public ActivityTestRule<Activity_DetailProdukMakeUp> activityRule = new ActivityTestRule<Activity_DetailProdukMakeUp>(Activity_DetailProdukMakeUp.class);

    @Test
    public void isinilai() throws Exception {
        onView(withId(R.id.jumlahbarang)).perform(typeText(nilai), closeSoftKeyboard());
        onView(withId(R.id.buttonbeli)).perform(click());
    }

}
