package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class AskSuccess extends AppCompatActivity {
    private ImageView mBtnBack;
    private Button mBtnCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_success);

        mBtnBack = (ImageView) findViewById(R.id.btnBack);

        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tanyaDokter = new Intent(getApplicationContext(),TanyaDokter.class);
                startActivity(tanyaDokter);
            }
        });

        mBtnCheck = (Button) findViewById(R.id.btnCheck);

        mBtnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inbox = new Intent(getApplicationContext(),Inbox.class);
                startActivity(inbox);
            }
        });
    }
}
