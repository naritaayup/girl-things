package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Belipakaianhijab extends AppCompatActivity {
    private ImageView cream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_belipakaianhijab);
        cream = (ImageView) findViewById(R.id.imageView6);
        cream.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toProduct();
            }
        });

        TextView offer = findViewById(R.id.search7);
        offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toOffer();
            }
        });
    }

    public void toProduct() {
        Intent intent = new Intent(this, Halamandetailproduct.class);
        startActivity(intent);
    }

    public void toOffer() {
        Intent intent = new Intent(this, Halnewofferpakaian.class);
        startActivity(intent);
    }
}
