package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Tanya extends AppCompatActivity {
    String nama, asal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tanya);
        nama = getIntent().getStringExtra("nama");
        asal = getIntent().getStringExtra("asal");

        TextView namaExpert = findViewById(R.id.tvNamaDesigner);
        TextView asalExpert = findViewById(R.id.tvAsal);

        namaExpert.setText(nama);
        asalExpert.setText(asal);
    }
}
