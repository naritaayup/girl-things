package com.example.girlthings.model;

import java.util.ArrayList;
import java.util.List;

public class DummyWeddingMakeUpArtistData {
    public static List<WeddingMakeUpArtistModel> weddingMakeUpArtistList() {
        WeddingMakeUpArtistModel wedding = new WeddingMakeUpArtistModel(1, "Siti Fatimah",
                "- Lokasi : Bandung Selatan \n- Jam Operasional : \n  * Senin - Kamis = 09.00 - 15.00 \n  * Jumat = 08.00 \n\n- Tarif : \n  1. Make up = Rp 25.000 \n  2. dll",
                "https://blushmagazine.ca/wp-content/uploads/2020/03/AlisonBenWedding-193-1095x700.jpg",
                "bandung selatan",
                "wedding",
                "",
                "",
                "",
                "");
        WeddingMakeUpArtistModel weddings = new WeddingMakeUpArtistModel(2, "Siti Badriah",
                "- Lokasi : Bandung Selatan \n- Jam Operasional : \n  * Senin - Kamis = 09.00 - 15.00 \n  * Jumat = 08.00 \n\n- Tarif : \n  1. Make up = Rp 25.000 \n  2. dll",
                "https://blushmagazine.ca/wp-content/uploads/2020/03/AlisonBenWedding-193-1095x700.jpg",
                "bandung selatan",
                "wedding",
                "",
                "",
                "",
                "");
        WeddingMakeUpArtistModel weddingss = new WeddingMakeUpArtistModel(3, "Siti Hajar",
                "- Lokasi : Bandung Utara \n- Jam Operasional : \n  * Senin - Kamis = 09.00 - 15.00 \n  * Jumat = 08.00 \n\n- Tarif : \n  1. Make up = Rp 25.000 \n  2. dll",
                "https://blushmagazine.ca/wp-content/uploads/2020/03/AlisonBenWedding-193-1095x700.jpg",
                "bandung utara",
                "wedding",
                "",
                "",
                "",
                "");
        WeddingMakeUpArtistModel weddingsss = new WeddingMakeUpArtistModel(4, "Siti Fajar",
                "- Lokasi : Bandung Barat \n- Jam Operasional : \n  * Senin - Kamis = 09.00 - 15.00 \n  * Jumat = 08.00 \n\n- Tarif : \n  1. Make up = Rp 25.000 \n  2. dll",
                "https://blushmagazine.ca/wp-content/uploads/2020/03/AlisonBenWedding-193-1095x700.jpg",
                "bandung barat",
                "wedding",
                "",
                "",
                "",
                "");
        WeddingMakeUpArtistModel weddingTwo = new WeddingMakeUpArtistModel(5, "Wirda Mansur",
                "- Lokasi : Bandung Barat \n- Jam Operasional : \n  * Senin - Kamis = 09.00 - 15.00 \n  * Jumat = 08.00 \n\n- Tarif : \n  1. Make up = Rp 25.000 \n  2. dll",
                "https://blushmagazine.ca/wp-content/uploads/2020/03/AlisonBenWedding-193-1095x700.jpg",
                "bandung barat",
                "wisuda",
                "",
                "",
                "",
                "");
        WeddingMakeUpArtistModel weddingTwos = new WeddingMakeUpArtistModel(6, "Wirda Salim",
                "- Lokasi : Bandung Barat \n- Jam Operasional : \n  * Senin - Kamis = 09.00 - 15.00 \n  * Jumat = 08.00 \n\n- Tarif : \n  1. Make up = Rp 25.000 \n  2. dll",
                "https://blushmagazine.ca/wp-content/uploads/2020/03/AlisonBenWedding-193-1095x700.jpg",
                "bandung barat",
                "wisuda",
                "",
                "",
                "",
                "");
        WeddingMakeUpArtistModel weddingThree = new WeddingMakeUpArtistModel(7, "Wardah Sari",
                "- Lokasi : Bandung Utara \n- Jam Operasional : \n  * Senin - Kamis = 09.00 - 15.00 \n  * Jumat = 08.00 \n\n- Tarif : \n  1. Make up = Rp 25.000 \n  2. dll",
                "https://blushmagazine.ca/wp-content/uploads/2020/03/AlisonBenWedding-193-1095x700.jpg",
                "bandung utara",
                "engagement",
                "",
                "",
                "",
                "");
        WeddingMakeUpArtistModel weddingThrees = new WeddingMakeUpArtistModel(6, "Wardah Tari",
                "- Lokasi : Bandung Utara \n- Jam Operasional : \n  * Senin - Kamis = 09.00 - 15.00 \n  * Jumat = 08.00 \n\n- Tarif : \n  1. Make up = Rp 25.000 \n  2. dll",
                "https://blushmagazine.ca/wp-content/uploads/2020/03/AlisonBenWedding-193-1095x700.jpg",
                "bandung utara",
                "engagement",
                "",
                "",
                "",
                "");

        List<WeddingMakeUpArtistModel> weddingMakeUpArtistModels = new ArrayList<>();

        weddingMakeUpArtistModels.add(wedding);
        weddingMakeUpArtistModels.add(weddings);
        weddingMakeUpArtistModels.add(weddingss);
        weddingMakeUpArtistModels.add(weddingsss);
        weddingMakeUpArtistModels.add(weddingTwo);
        weddingMakeUpArtistModels.add(weddingTwos);
        weddingMakeUpArtistModels.add(weddingThree);
        weddingMakeUpArtistModels.add(weddingThrees);

        return weddingMakeUpArtistModels;
    }
}
