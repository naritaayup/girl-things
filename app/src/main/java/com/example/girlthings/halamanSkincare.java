package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class halamanSkincare extends AppCompatActivity {

    private EditText search;
    private ImageView cari;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman_skincare);
        search = findViewById(R.id.editText4);
        cari = (ImageView) findViewById(R.id.imageView22);
        cari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toSearch();
            }
        });

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("message");

        myRef.setValue("Hello, World!");
    }

    public void moisturizer(View view) {
        Intent a = new Intent(halamanSkincare.this, halamanSkincare2.class);
        startActivity(a);
    }

    public void toSearch() {
        Intent intent = new Intent(this, cariSkincare.class);
        startActivity(intent);
    }
}
