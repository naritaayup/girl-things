package com.example.girlthings;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.girlthings.model.ConfirmationModel;
import com.example.girlthings.model.UserModel;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

public class Halamankonfirmasibayar extends AppCompatActivity {
    TextView next, ch;
    String bank, harga, namaB;
    StorageReference mStorageRef;
    ImageView img;
    public Uri imguri;
    private StorageTask uploadTask;

    private FirebaseAuth mAuth;
    private FirebaseDatabase fDb;
    private DatabaseReference dbRef;

    private ConfirmationModel consModel;
    private UserModel userModel;

    private String uid;
    private long id;
    private String username;
    private int transId;
    private int ids;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halamankonfirmasibayar);

        next = (TextView) findViewById(R.id.textView37);
        ch = (TextView) findViewById(R.id.choose);
        mStorageRef = FirebaseStorage.getInstance().getReference().child("TransactionImageFolder");
        img = findViewById(R.id.imageView4);

        TextView namaBank = findViewById(R.id.textView32);
        TextView totalHarga = findViewById(R.id.textView34);



        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(uploadTask != null && uploadTask.isInProgress()){
                    Toast.makeText(Halamankonfirmasibayar.this, "Sedang di Upload", Toast.LENGTH_LONG).show();
                }else{
                    toberhasil();
                }
            }
        }
        );

        ids = getIntent().getIntExtra("ids", 0);
        transId = getIntent().getIntExtra("transId", 0);
        username = getIntent().getStringExtra("username");

        Intent intent = getIntent();
        bank = intent.getStringExtra(Pembayaran.EXTRA_MESSAGE);
        harga = intent.getStringExtra(Pembayaran.EXTRA_MESSAGE_2);
        namaB = intent.getStringExtra(Pembayaran.itemBarang);

        namaBank.setText(bank);
        totalHarga.setText("Rp. "+harga);

        ch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Filechooser();
            }
        });

        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference();
    }

    public void toberhasil() {
        final StorageReference Ref = mStorageRef.child("image" + imguri.getLastPathSegment());
        Ref.putFile(imguri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        consModel = new ConfirmationModel(transId, "Pending", String.valueOf(uri), username, harga, namaB);
                        dbRef.child("transaksiStatus").child(ids + "").setValue(consModel);
                    }
                });
            }
        });

        Intent intent = new Intent(this, Berhasilkonfirmasi.class);
        startActivity(intent);
    }

    public void Filechooser(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && resultCode==RESULT_OK && data!=null && data.getData() !=null){
            imguri = data.getData();
            img.setImageURI(imguri);
        }
    }

    private String getExtension(Uri uri){
        ContentResolver cr = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(cr.getType(uri));
    }


}
