package com.example.girlthings;

import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class masukoginTest {

    private String email = "naritaayup@gmail.com";
    private String emailda = "naritaayup";
    private String password = "narita123";

    @Rule
    public ActivityTestRule<masukogin> activityTestRule = new ActivityTestRule<>(masukogin.class);

    @Test
    public void loginSuccess() throws Exception {
        onView(withId(R.id.editTextEmail)).perform(typeText(email), closeSoftKeyboard());
        onView(withId(R.id.editTextPass)).perform(typeText(password), closeSoftKeyboard());
    }

    @Test
    public void loginFailed() throws Exception {
        onView(withId(R.id.editTextEmail)).perform(typeText(emailda), closeSoftKeyboard());
        onView(withId(R.id.editTextPass)).perform(typeText(password), closeSoftKeyboard());
    }

}