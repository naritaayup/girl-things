package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class halamanSkincare2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman_skincare2);
    }

    public void beli(View view) {
        Intent b = new Intent(halamanSkincare2.this, beliSkincare.class);
        startActivity(b);
    }

    public void backhalsatu(View view) {
        Intent a = new Intent(halamanSkincare2.this, halamanSkincare.class);
        startActivity(a);
    }
}
