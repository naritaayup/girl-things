package com.example.girlthings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.girlthings.model.ConfirmationModel;
import com.example.girlthings.model.TransactionModel;
import com.example.girlthings.model.UserModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.NumberFormat;
import java.util.Locale;

public class ConfirmationPageActivity extends AppCompatActivity {

    private TextView textViewFill;
    private TextView textViewNoRekValue;
    private TextView textViewTotalTagihanValue;

    private FirebaseAuth mAuth;
    private FirebaseDatabase fDb;
    private DatabaseReference dbRef;
    private TransactionModel transModel;
    private ConfirmationModel consModel;
    private UserModel userModel;

    private String methodPayment;
    private String price;
    private int id;
    private String currentTime;
    private String uid;
    private long transId;
    private long confId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmastion_page);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        mAuth = FirebaseAuth.getInstance();
        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference();

        textViewFill = findViewById(R.id.textViewFill);
        textViewNoRekValue = findViewById(R.id.textViewNoRekValue);
        textViewTotalTagihanValue = findViewById(R.id.textViewTotalTagihanValue);

        uid = mAuth.getUid();
        methodPayment = getIntent().getStringExtra("methodPayment");
        price = getIntent().getStringExtra("price");
        id = getIntent().getIntExtra("id", 0);
        currentTime = getIntent().getStringExtra("currentTime");

        dbRef.child("transaction").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                transId = dataSnapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Do Nothing
            }
        });

        dbRef.child("users").child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                   userModel = dataSnapshot.getValue(UserModel.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Do Nothing
            }
        });

        dbRef.child("transaksiStatus").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                confId = dataSnapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        if (methodPayment.matches("Mandiri")) {
            textViewFill.setText("Transfer " + methodPayment);
            textViewNoRekValue.setText("147-1000-2456-1231");
        } else if (methodPayment.matches("BCA")) {
            textViewFill.setText("Transfer " + methodPayment);
            textViewNoRekValue.setText("256-2000-421-9987");
        } else {
            textViewFill.setText("Transfer " + methodPayment);
            textViewNoRekValue.setText("678-4000-1294-4215");
        }

        textViewTotalTagihanValue.setText(changeRupiah(Double.valueOf(price)));
    }

    public String changeRupiah(Number harga) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String stringHarga = formatRupiah.format(harga);
        stringHarga = "Rp. " + stringHarga.substring(stringHarga.indexOf('p') + 1);
        return stringHarga;
    }

    public void confirmationPayment(View view) {
        int transactionId = (int) transId + 1;
        transModel = new TransactionModel(uid, id, methodPayment, currentTime, price);
        dbRef.child("transaction").child(transactionId+"").setValue(transModel);

        int confsId = (int) confId + 1;
        consModel = new ConfirmationModel(transactionId, "Belum Dibayar", "", userModel.getName(), price, "Make Up Artist");
        dbRef.child("transaksiStatus").child(confsId+"").setValue(consModel);

        Intent intent = new Intent(this, ConfirmationPaymentActivity.class);
        intent.putExtra("transId", transactionId);
        intent.putExtra("confsId", confsId);
        intent.putExtra("username", userModel.getName());
        intent.putExtra("price", price);
        startActivity(intent);
        Toast.makeText(this, "Transaksi berhasil, silahkan melakukan konfirmasi", Toast.LENGTH_LONG).show();
        finish();
    }

    public void onBackPressed(View view) {
        finish();
    }
}
