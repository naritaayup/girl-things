package com.example.girlthings.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;

import com.example.girlthings.Constant;
import com.example.girlthings.EditProfile;
import com.example.girlthings.HomeActivity;
import com.example.girlthings.KebijakanPrivasi;
import com.example.girlthings.LoginRegisterView;
import com.example.girlthings.NotifSettings;
import com.example.girlthings.R;
import com.example.girlthings.RateApp;
import com.example.girlthings.SharedPref;
import com.example.girlthings.SyaratKetentuan;
import com.example.girlthings.TentangAplikasi;
import com.example.girlthings.model.UserModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.orhanobut.hawk.Hawk;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    private TextView mTvNotif, mTvEdit, mTvRate, mTvTentang, mTvSyarat, mTvKebijakan;
    private Switch myswitch;
    SharedPref sharedpref;
    private TextView tvNama, tvNoTelp, tvEmail;
    private Button btnLogout;

    private FirebaseAuth mAuth;
    private FirebaseDatabase fDb;
    private DatabaseReference dbRef;
    private UserModel user;

    private String uid;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference();

        uid = mAuth.getUid();

        tvNama = (TextView) getActivity().findViewById(R.id.tvNama);
        tvNoTelp = (TextView) getActivity().findViewById(R.id.tvNoTelp);
        tvEmail = (TextView) getActivity().findViewById(R.id.tvEmail);

        dbRef.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    user = dataSnapshot.child(uid).getValue(UserModel.class);
                    tvNama.setText(Hawk.get(Constant.USER).toString());
                    tvNoTelp.setText(Hawk.get(Constant.PHONE).toString());
                    tvEmail.setText(Hawk.get(Constant.EMAIL).toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Do Nothing
            }
        });

        mTvNotif = (TextView) getActivity().findViewById(R.id.tvNotif);
        mTvNotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent notifSettings = new Intent(getContext(), NotifSettings.class);
                startActivity(notifSettings);
            }
        });

        mTvEdit = (TextView) getActivity().findViewById(R.id.tvEdit);
        mTvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editProfile = new Intent(getContext(), EditProfile.class);
                startActivity(editProfile);
            }
        });

        mTvRate = (TextView) getActivity().findViewById(R.id.tvRate);
        mTvRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent rateApp = new Intent(getContext(), RateApp.class);
                startActivity(rateApp);
            }
        });

        mTvTentang = (TextView) getActivity().findViewById(R.id.tvTentang);
        mTvTentang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tentangAplikasi = new Intent(getContext(), TentangAplikasi.class);
                startActivity(tentangAplikasi);
            }
        });

        mTvSyarat = (TextView) getActivity().findViewById(R.id.tvSyarat);
        mTvSyarat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent syaratKetentuan = new Intent(getContext(), SyaratKetentuan.class);
                startActivity(syaratKetentuan);
            }
        });

        mTvKebijakan = (TextView) getActivity().findViewById(R.id.tvKebijakan);
        mTvKebijakan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent kebijakanPrivasi = new Intent(getContext(), KebijakanPrivasi.class);
                startActivity(kebijakanPrivasi);
            }
        });

        btnLogout = getActivity().findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Hawk.deleteAll();
                mAuth.signOut();
                startActivity(new Intent(getContext(), LoginRegisterView.class));
            }
        });

        sharedpref = new SharedPref(getContext());
        if(sharedpref.loadNightModeState()==true) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        myswitch=(Switch) getActivity().findViewById(R.id.switch2);
        if (sharedpref.loadNightModeState()==true) {
            myswitch.setChecked(true);
        }
        myswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sharedpref.setNightModeState(true);
                    restartApp();
                }
                else {
                    sharedpref.setNightModeState(false);
                    restartApp();
                }
            }
        });
    }

    public void restartApp () {
        Intent i = new Intent(getContext(), HomeActivity.class);
        startActivity(i);
        getActivity().finish();
    }
}
