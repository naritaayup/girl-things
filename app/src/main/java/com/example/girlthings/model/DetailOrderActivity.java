package com.example.girlthings.model;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.girlthings.MethodPayment;
import com.example.girlthings.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DetailOrderActivity extends AppCompatActivity {

    private TextView textViewNameValue;
    private TextView textViewAddressValue;
    private TextView textViewNoTelpValue;
    private TextView textViewJamPesanValue;
    private TextView textViewHargaValue;
    private TextView textViewTotalHargaValue;

    private FirebaseAuth mAuth;
    private FirebaseDatabase fDb;
    private DatabaseReference dbRef;
    private UserModel user;

    private String price;
    private int id;
    private String uid;
    private String currentTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_order);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        mAuth = FirebaseAuth.getInstance();
        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference("users");

        textViewNameValue = findViewById(R.id.textViewNameValue);
        textViewAddressValue = findViewById(R.id.textViewAddressValue);
        textViewNoTelpValue = findViewById(R.id.textViewNoTelpValue);
        textViewJamPesanValue = findViewById(R.id.textViewJamPesanValue);
        textViewHargaValue = findViewById(R.id.textViewHargaValue);
        textViewTotalHargaValue = findViewById(R.id.textViewTotalHargaValue);

        uid = mAuth.getUid();
        price = getIntent().getStringExtra("price");
        id = getIntent().getIntExtra("id", 0);

        currentTime = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.getDefault()).format(new Date());

        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    user = dataSnapshot.child(uid).getValue(UserModel.class);
                    textViewNameValue.setText(user.getName());
                    textViewAddressValue.setText(user.getAddress());
                    textViewNoTelpValue.setText(user.getPhoneNumber());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Do Nothing
            }
        });

        textViewJamPesanValue.setText(currentTime);
        textViewHargaValue.setText(changeRupiah(Double.valueOf(price)));
        textViewTotalHargaValue.setText(changeRupiah(Double.valueOf(price)));
    }

    public String changeRupiah(Number harga){
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String stringHarga = formatRupiah.format(harga);
        stringHarga = "Rp. "+stringHarga.substring(stringHarga.indexOf('p')+1);
        return stringHarga;
    }

    public void onBackPressed(View view) {
        finish();
    }

    public void methodPayment(View view) {
        Intent intent = new Intent(this, MethodPayment.class);
        intent.putExtra("price", price);
        intent.putExtra("id", id);
        intent.putExtra("currentTime", currentTime);
        startActivity(intent);
    }
}
