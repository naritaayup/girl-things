package com.example.girlthings.model;

public class ConfirmationModel {
    private int idTra;
    private String status;
    private String image;
    private String username;
    private String total;
    private String barang;

    public ConfirmationModel() {

    }

    public ConfirmationModel(int transactionId, String status, String image, String username, String total, String barang) {
        this.idTra = transactionId;
        this.status = status;
        this.image = image;
        this.username = username;
        this.total = total;
        this.barang = barang;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getIdTra() {
        return idTra;
    }

    public void setIdTra(int idTra) {
        this.idTra = idTra;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getBarang() {
        return barang;
    }

    public void setBarang(String barang) {
        this.barang = barang;
    }
}
