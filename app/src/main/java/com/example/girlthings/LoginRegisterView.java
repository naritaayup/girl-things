package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;

public class LoginRegisterView extends AppCompatActivity {

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_register_view);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null && mAuth.getUid() != null) {
            startActivity(new Intent(LoginRegisterView.this, HomeActivity.class));
        }
    }

    public void goToLogin(View view) {
        startActivity(new Intent(this, masukogin.class));
    }

    public void goToRegister(View view) {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mAuth.getCurrentUser() != null && mAuth.getUid() != null) {
            startActivity(new Intent(LoginRegisterView.this, HomeActivity.class));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAuth.getCurrentUser() != null && mAuth.getUid() != null) {
            startActivity(new Intent(LoginRegisterView.this, HomeActivity.class));
        }
    }
}
