package com.example.girlthings;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.girlthings.model.DetailOrderActivity;
import com.example.girlthings.model.UserModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.orhanobut.hawk.Hawk;

public class DetailOrderDataCustomerActivity extends AppCompatActivity {

    private TextView textViewNameValue;
    private TextView textViewAddressValue;
    private TextView textViewNoTelpValue;
    private Button btnOrder;

    private FirebaseAuth mAuth;
    private FirebaseDatabase fDb;
    private DatabaseReference dbRef;
    private UserModel user;

    private String uid;
    private String price;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_order_data_customer);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        mAuth = FirebaseAuth.getInstance();
        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference("users");

        textViewNameValue = findViewById(R.id.textViewNameValue);
        textViewAddressValue = findViewById(R.id.textViewAddressValue);
        textViewNoTelpValue = findViewById(R.id.textViewNoTelpValue);
        btnOrder = findViewById(R.id.btnOrder);

        price = getIntent().getStringExtra("price");
        id = getIntent().getIntExtra("id", 0);
        uid = mAuth.getUid();

        textViewNameValue.setText(Hawk.get(Constant.USER).toString());
        textViewAddressValue.setText(Hawk.get(Constant.ADDRESS).toString());
        textViewNoTelpValue.setText(Hawk.get(Constant.PHONE).toString());

        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailOrderDataCustomerActivity.this, DetailOrderActivity.class);
                intent.putExtra("price", price);
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });
    }

    public void onBackPressed(View view) {
        finish();
    }
}
