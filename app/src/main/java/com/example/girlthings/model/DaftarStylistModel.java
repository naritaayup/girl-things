package com.example.girlthings.model;

import com.example.girlthings.DaftarStylist;

public class DaftarStylistModel {
    public String nama;
    public String image;
    public int id;
    public String expert;
    public String asal;

    public DaftarStylistModel(String nama, String image, int id, String expert, String asal){
        this.nama = nama;
        this.image = image;
        this.id = id;
        this.expert = expert;
        this.asal = asal;

    }
    public DaftarStylistModel(){

    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getExpert() {
        return expert;
    }

    public void setExpert(String expert) {
        this.expert = expert;
    }

    public String getAsal() {
        return asal;
    }

    public void setAsal(String asal) {
        this.asal = asal;
    }
}
