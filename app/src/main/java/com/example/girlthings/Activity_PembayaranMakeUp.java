package com.example.girlthings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.girlthings.Activity_DetailProdukMakeUp.EXTRA_MESSAGE;

public class Activity_PembayaranMakeUp extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.android.twoactivities.extra.MESSAGE";
    private TextView totalharga;

    Integer angka, total;
    Button Metode;
    EditText address;

    String Alamat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembayaran_make_up);

        totalharga = (TextView)findViewById(R.id.jumlahharga);

        Metode = (Button)findViewById(R.id.buttonmetodepembayaran);
        Metode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MetodePembayaran();
            }
        });

        //inisiasi
        Intent intent = getIntent();
        String jumlah = intent.getStringExtra(EXTRA_MESSAGE);

        //mengambil id jumlah barang
        TextView textView = findViewById(R.id.jumlahbarang);
        textView.setText(jumlah);

        //menghitung harga barang dan menampilkan barang
        angka = Integer.parseInt(jumlah);
        total = (angka*75000)+30000;
        totalharga.setText("Rp" + total);

        //address
        address = findViewById(R.id.isialamat);
        Alamat = address.getText().toString();

    }
    public void MetodePembayaran() {
        if (address.getText().toString().matches("")) {
            Toast toast = Toast.makeText(getApplicationContext(), "Mohon isikan alamat", Toast.LENGTH_SHORT);
            toast.show();
        } else {
            String coba = String.valueOf(total);
            Intent intent = new Intent(this, Pembayaran.class);
            intent.putExtra(EXTRA_MESSAGE, coba);
            startActivity(intent);
        }
    }

    public void save(View view) {
        if (address.getText().toString().matches("")){
            Toast toast = Toast.makeText(getApplicationContext(), "Mohon isikan alamat", Toast.LENGTH_SHORT);
            toast.show();
        }else{
            Toast toast = Toast.makeText(getApplicationContext(), "Alamat Berhasil Disimpan", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
