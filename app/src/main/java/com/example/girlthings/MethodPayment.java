package com.example.girlthings;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;

import java.text.NumberFormat;
import java.util.Locale;

public class MethodPayment extends AppCompatActivity {

    private TextView textViewTotalHargaValue;

    private String methodPayment;
    private String price;
    private int id;
    private String currentTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_method_payment);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        price = getIntent().getStringExtra("price");
        id = getIntent().getIntExtra("id", 0);
        currentTime = getIntent().getStringExtra("currentTime");

        textViewTotalHargaValue = findViewById(R.id.textViewTotalHargaValue);

        textViewTotalHargaValue.setText(changeRupiah(Double.valueOf(price)));
    }

    public String changeRupiah(Number harga){
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String stringHarga = formatRupiah.format(harga);
        stringHarga = "Rp. "+stringHarga.substring(stringHarga.indexOf('p')+1);
        return stringHarga;
    }

    public void onBackPressed(View view) {
        finish();
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((AppCompatRadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.tfMandiri:
                if (checked) {
                    methodPayment = "Mandiri";
                }
                break;
            case R.id.tfBCA:
                if (checked) {
                    methodPayment = "BCA";
                }
                break;
            case R.id.tfBri:
                if (checked) {
                    methodPayment = "BRI";
                }
                break;
            default:
                // Do Nothing
                break;
        }
    }

    public void payment(View view) {
        Intent intent = new Intent(this, ConfirmationPageActivity.class);
        intent.putExtra("price", price);
        intent.putExtra("id", id);
        intent.putExtra("currentTime", currentTime);
        intent.putExtra("methodPayment", methodPayment);
        startActivity(intent);
    }
}
