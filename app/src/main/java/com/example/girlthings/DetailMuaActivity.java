package com.example.girlthings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.girlthings.adapter.ReviewMuaAdapter;
import com.example.girlthings.model.ReviewModel;
import com.example.girlthings.model.UserModel;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DetailMuaActivity extends AppCompatActivity {

    private TextView textViewNames;
    private TextView textViewDesc;
    private TextView prices;
    private TextView bukatutup;
    private TextView addresses;
    private TextView number_reviews;
    private TextView statuss;
    private ImageView imageViewProfile;
    private Button btnChat;
    private Button btnOrder;
    private ImageView review;
    private RecyclerView scrollableview;
    private Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbar;
    private AppBarLayout appBarLayout;

    private Dialog dialog;
    private FirebaseAuth mAuth;
    private FirebaseDatabase fDb;
    private DatabaseReference dbRef;
    private List<ReviewModel> model = new ArrayList<>();
    private ReviewModel reviewModel;
    private ReviewMuaAdapter adapter;
    private UserModel user;

    private String uid;
    private int id;
    private String username;
    private String userImage;
    private RatingBar totalStar;
    private long reviewId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_mua);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        id = getIntent().getIntExtra("id", 0);
        final String name = getIntent().getStringExtra("name");
        String desc = getIntent().getStringExtra("desc");
        final String image = getIntent().getStringExtra("image");
        String status = getIntent().getStringExtra("status");
        final String telp = getIntent().getStringExtra("telp");
        String address = getIntent().getStringExtra("address");
        final String price = getIntent().getStringExtra("price");

        mAuth = FirebaseAuth.getInstance();
        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference();

        uid = mAuth.getUid();

        toolbar = findViewById(R.id.anim_toolbar);
        collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        appBarLayout = findViewById(R.id.appbar);
        textViewNames = findViewById(R.id.textViewNames);
        textViewDesc = findViewById(R.id.textViewDesc);
        prices = findViewById(R.id.prices);
        bukatutup = findViewById(R.id.bukatutup);
        addresses = findViewById(R.id.address);
        statuss = findViewById(R.id.status);
        imageViewProfile = findViewById(R.id.imageViewProfile);
        btnChat = findViewById(R.id.btnChat);
        btnOrder = findViewById(R.id.btnOrder);
        review = findViewById(R.id.review);
        scrollableview = findViewById(R.id.scrollableview);
        totalStar = findViewById(R.id.total_star);
        number_reviews = findViewById(R.id.number_reviews);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        textViewNames.setText(name);
        textViewDesc.setText(desc);
        statuss.setText("(" + status + ")");
        prices.setText(changeRupiah(Double.valueOf(price)));
        addresses.setText(address);
        Picasso.with(getApplicationContext()).load(image).into(imageViewProfile);

        dbRef.child("review").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                reviewId = dataSnapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        dbRef.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                user = dataSnapshot.child(uid).getValue(UserModel.class);
                username = user.getName();
                userImage = user.getImage();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Do Nothing
            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        scrollableview.setLayoutManager(llm);

        adapter = new ReviewMuaAdapter(DetailMuaActivity.this);
        scrollableview.setAdapter(adapter);

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://api.whatsapp.com/send?phone=" + "62" + telp;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailMuaActivity.this, DetailOrderDataCustomerActivity.class);
                intent.putExtra("price", price);
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });

        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                messageDialog();
            }
        });

        showReviewData();
    }

    private void showReviewData() {
        dbRef.child("review").orderByChild("imageId").equalTo(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        ReviewModel models = ds.getValue(ReviewModel.class);
                        model.add(models);
                    }
                    adapter.setItems(model);
                    adapter.notifyDataSetChanged();

                    float sumRate = 0;
                    for (ReviewModel reviewModel : model) {
                        sumRate += Integer.valueOf(reviewModel.getRating());
                    }

                    float avgRate = sumRate / model.size();
                    totalStar.setRating(avgRate);
                    number_reviews.setText(model.size() + " reviews");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Do Nothing
            }
        });
    }

    private void messageDialog() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_review);
        final RatingBar valueRating = dialog.findViewById(R.id.value_ratting);
        final EditText valueReview = dialog.findViewById(R.id.value_review);
        Button btnReview = dialog.findViewById(R.id.btn_review);

        btnReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int intValueRating = (int) valueRating.getRating();
                String review = valueReview.getText().toString();
                int ids = (int) reviewId+1;
                reviewModel = new ReviewModel(uid, String.valueOf(intValueRating), review, username, userImage, id);
                dbRef.child("review").child(ids+"").setValue(reviewModel);
                dialog.dismiss();
                showReviewData();
            }
        });

        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    public String changeRupiah(Number harga) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String stringHarga = formatRupiah.format(harga);
        stringHarga = "Rp. " + stringHarga.substring(stringHarga.indexOf('p') + 1);
        return stringHarga;
    }

    public void onBackPressed(View view) {
        finish();
    }
}
